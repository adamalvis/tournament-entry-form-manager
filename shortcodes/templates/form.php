<div class="tournament-form">
	<form action="<?= PLUGIN_DIR ?>actions/form-save.php" id="oc-tournament-form" class="tournament-form" method="POST">
		
		<?php if ( isset( $_SESSION['transactions'] ) && empty( $_SESSION['transactions'] ) !== true ): ?>
			<div class="prior-transactions-box">
				<p><strong>Prior Team(s) Total Fees:</strong> <?= $controller->get_prior_team_fees() ?></p>
				<p class="footnote">(Excludes processing &amp; late fees)</p>
			</div>
		<?php endif; ?>

		<section <?php if ( ! isset( $_SESSION['organizer'] ) ): ?>class="first"<?php endif; ?>>
			<div class="row">
				<div class="full tournament-rules">
					<?= apply_filters('the_content', $post->post_content) ?>
				</div>
			</div>
			<div class="row">
				<div class="full">
					<span class="checkbox">
						<input type="checkbox" value="yes" id="read-rules" name="read-rules" class="required"> I have read the tournament rules
					</span>
				</div>
			</div>
			<div class="row">
				<div class="half">
					<a href="#" class="btn print-rules">Print Rules</a>
				</div>
				<div class="half">
					<a href="#" class="next">Continue</a>
				</div>
			</div>
			<div class="row">
				<div class="full">
					<div class="form-errors"></div>
				</div>
			</div>
		</section>

		<section id="registrant" <?php if ( isset( $_SESSION['organizer'] ) ): ?>class="first"<?php endif; ?>>
			<div class="row">
				<div class="full">
					<h2>Team Captain / Squad Organizer</h2>
				</div>
			</div>
			<input type="hidden" id="heading-registrant" name="heading-registrant" value="Team Captain / Squad Organizer">
			<div class="gray-back">
				<div class="row">
					<div class="half">
						<label for="full-name">Full Name*</label>
						<input class="required" type="text" id="full-name" name="full-name" value="<?= $controller->organizer('full-name') ?>">
					</div>
					<div class="half">
						<label for="reservation-number">Reservation Number</label>
						<input type="text" id="reservation-number" name="reservation-number" value="<?= $controller->organizer('reservation-number') ?>">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="address">Address*</label>
						<input class="required" type="text" id="address" name="address" value="<?= $controller->organizer('address') ?>">
					</div>
					<div class="half">
						<label for="city">City*</label>
						<input class="required" type="text" id="city" name="city" value="<?= $controller->organizer('city') ?>">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="state">State*</label>
						<input class="required" type="text" id="state" name="state" value="<?= $controller->organizer('state') ?>">
					</div>
					<div class="half">
						<label for="zip-code">Zip Code*</label>
						<input class="required" type="text" id="zip-code" name="zip-code" value="<?= $controller->organizer('zip-code') ?>">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="evening-phone">Evening Phone*</label>
						<input class="required" type="text" id="evening-phone" name="evening-phone" value="<?= $controller->organizer('evening-phone') ?>">
					</div>
					<div class="half">
						<label for="daytime-phone">Daytime Phone*</label>
						<input class="required" type="text" id="daytime-phone" name="daytime-phone" value="<?= $controller->organizer('daytime-phone') ?>">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="email">Email*</label>
						<input class="required" type="text" id="email" name="email" value="<?= $controller->organizer('email') ?>">
					</div>
					<div class="half">
						<label for="local-association">Local Association*</label>
						<select class="required" type="text" id="local-association" name="local-association">
							<option value=""></option>
							<?php foreach ($controller->local_associations() as $association): ?>
								<?php var_dump($controller->organizer('local-association')); ?>
								<?php var_dump($association); ?>
								<option 
									<?php if ($association == $controller->organizer('local-association')): ?>selected="selected"<?php endif; ?>
									value="<?= $association ?>"><?= $association ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="full">
						<p>I am signing up:</p>
						<select name="signup-type" id="signup-type" class="required">
							<option value=""></option>
							<option value="both">Team &amp; Doubles / Singles</option>
							<option value="team">Team Only</option>
							<option value="doubles">Doubles Team / Singles Only</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="half">
					<a href="#" class="prev">Previous Step</a>
				</div>
				<div class="half">
					<a href="#" class="next">Next Step</a>
				</div>
			</div>
			<div class="row">
				<div class="full">
					<div class="form-errors"></div>
				</div>
			</div>
		</section>

		<section class="bowlers" id="bowlers">
			<div class="row">
				<div class="full">
					<h2>Bowler Information</h2>
				</div>
			</div>

			<input type="hidden" id="heading-bowlers" name="heading-bowlers" value="Bowlers">

			<!-- BOWLER #1 -->
			<div class="bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #1</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-1-national-id">National ID</label>
						<input type="text" id="bowler-1-national-id" name="bowler-1-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-1-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-1-name" name="bowler-1-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-1-email">Email Address</label>
						<input type="text" id="bowler-1-email" name="bowler-1-email" value="">
					</div>
					<div class="half">
						<label for="bowler-1-address">Address</label>
						<input type="text" id="bowler-1-address" name="bowler-1-address" value="">
					</div>
				</div>

				<div class="row">
					<div class="half">
						<label for="bowler-1-city">City</label>
						<input type="text" id="bowler-1-city" name="bowler-1-city" value="">
					</div>
					<div class="half">
						<label for="bowler-1-state">State</label>
						<input type="text" id="bowler-1-state" name="bowler-1-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-1-zip">Zip Code</label>
						<input type="text" id="bowler-1-zip" name="bowler-1-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-1-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-1-average" name="bowler-1-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-1-pba">PBA Bowler?</label>
						<select name="bowler-1-pba" id="bowler-1-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-1-other">Select all that apply: <span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-1-other" id="bowler-1-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-1-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-1-former-champion-event" name="bowler-1-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-1-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-1-manager-association" name="bowler-1-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-1-role">Describe your role</label>
						<textarea class="not-required" id="bowler-1-role" name="bowler-1-role"></textarea>
					</div>
				</div>
			</div>

			<!-- BOWLER #2 -->
			<div class="bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #2</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-2-national-id">National ID</label>
						<input type="text" id="bowler-2-national-id" name="bowler-2-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-2-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-2-name" name="bowler-2-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-2-email">Email Address</label>
						<input type="text" id="bowler-2-email" name="bowler-2-email" value="">
					</div>
					<div class="half">
						<label for="bowler-2-address">Address</label>
						<input type="text" id="bowler-2-address" name="bowler-2-address" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-2-city">City</label>
						<input type="text" id="bowler-2-city" name="bowler-2-city" value="">
					</div>
					<div class="half">
						<label for="bowler-2-state">State</label>
						<input type="text" id="bowler-2-state" name="bowler-2-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-2-zip">Zip Code</label>
						<input type="text" id="bowler-2-zip" name="bowler-2-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-2-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-2-average" name="bowler-2-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-2-pba">PBA Bowler?</label>
						<select name="bowler-2-pba" id="bowler-2-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-2-other">Select all that apply:<span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-2-other" id="bowler-2-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-2-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-2-former-champion-event" name="bowler-2-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-2-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-2-manager-association" name="bowler-2-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-2-role">Describe your role</label>
						<textarea class="not-required" id="bowler-2-role" name="bowler-2-role"></textarea>
					</div>
				</div>
			</div>

			<!-- BOWLER #3 -->
			<div class="bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #3</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-3-national-id">National ID</label>
						<input type="text" id="bowler-3-national-id" name="bowler-3-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-3-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-3-name" name="bowler-3-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-3-email">Email Address</label>
						<input type="text" id="bowler-3-email" name="bowler-3-email" value="">
					</div>
					<div class="half">
						<label for="bowler-3-address">Address</label>
						<input type="text" id="bowler-3-address" name="bowler-3-address" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-3-city">City</label>
						<input type="text" id="bowler-3-city" name="bowler-3-city" value="">
					</div>
					<div class="half">
						<label for="bowler-3-state">State</label>
						<input type="text" id="bowler-3-state" name="bowler-3-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-3-zip">Zip Code</label>
						<input type="text" id="bowler-3-zip" name="bowler-3-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-3-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-3-average" name="bowler-3-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-3-pba">PBA Bowler?</label>
						<select name="bowler-3-pba" id="bowler-3-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-3-other">Select all that apply:<span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-3-other" id="bowler-3-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-3-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-3-former-champion-event" name="bowler-3-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-3-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-3-manager-association" name="bowler-3-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-3-role">Describe your role</label>
						<textarea class="not-required" id="bowler-3-role" name="bowler-3-role"></textarea>
					</div>
				</div>
			</div>

			<!-- BOWLER #4 -->
			<div class="bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #4</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-4-national-id">National ID</label>
						<input type="text" id="bowler-4-national-id" name="bowler-4-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-4-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-4-name" name="bowler-4-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-4-email">Email Address</label>
						<input type="text" id="bowler-4-email" name="bowler-4-email" value="">
					</div>
					<div class="half">
						<label for="bowler-4-address">Address</label>
						<input type="text" id="bowler-4-address" name="bowler-4-address" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-4-city">City</label>
						<input type="text" id="bowler-4-city" name="bowler-4-city" value="">
					</div>
					<div class="half">
						<label for="bowler-4-state">State</label>
						<input type="text" id="bowler-4-state" name="bowler-4-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-4-zip">Zip Code</label>
						<input type="text" id="bowler-4-zip" name="bowler-4-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-4-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-4-average" name="bowler-4-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-4-pba">PBA Bowler?</label>
						<select name="bowler-4-pba" id="bowler-4-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-4-other">Select all that apply:<span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-4-other" id="bowler-4-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-4-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-4-former-champion-event" name="bowler-4-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-4-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-4-manager-association" name="bowler-4-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-4-role">Describe your role</label>
						<textarea class="not-required" id="bowler-4-role" name="bowler-4-role"></textarea>
					</div>
				</div>
			</div>

			<!-- BOWLER #5 -->
			<div class="bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #5</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-5-national-id">National ID</label>
						<input type="text" id="bowler-5-national-id" name="bowler-5-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-5-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-5-name" name="bowler-5-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-5-email">Email Address</label>
						<input type="text" id="bowler-5-email" name="bowler-5-email" value="">
					</div>
					<div class="half">
						<label for="bowler-5-address">Address</label>
						<input type="text" id="bowler-5-address" name="bowler-5-address" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-5-city">City</label>
						<input type="text" id="bowler-5-city" name="bowler-5-city" value="">
					</div>
					<div class="half">
						<label for="bowler-5-state">State</label>
						<input type="text" id="bowler-5-state" name="bowler-5-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-5-zip">Zip Code</label>
						<input type="text" id="bowler-5-zip" name="bowler-5-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-5-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-5-average" name="bowler-5-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-5-pba">PBA Bowler?</label>
						<select name="bowler-5-pba" id="bowler-5-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-5-other">Select all that apply:<span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-5-other" id="bowler-5-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-5-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-5-former-champion-event" name="bowler-5-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-5-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-5-manager-association" name="bowler-5-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-5-role">Describe your role</label>
						<textarea class="not-required" id="bowler-5-role" name="bowler-5-role"></textarea>
					</div>
				</div>
			</div>

			<!-- BOWLER #6 -->
			<div class="bowler" id="extra-bowler">
				<div class="row">
					<div class="full">
						<h3>Bowler #6</h3>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-6-national-id">National ID</label>
						<input type="text" id="bowler-6-national-id" name="bowler-6-national-id" value="">
					</div>
					<div class="half">
						<label for="bowler-6-name">Name</label>
						<input type="text" class="bowler-name" id="bowler-6-name" name="bowler-6-name" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-6-email">Email Address</label>
						<input type="text" id="bowler-6-email" name="bowler-6-email" value="">
					</div>
					<div class="half">
						<label for="bowler-6-address">Address</label>
						<input type="text" id="bowler-6-address" name="bowler-6-address" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-6-city">City</label>
						<input type="text" id="bowler-6-city" name="bowler-6-city" value="">
					</div>
					<div class="half">
						<label for="bowler-6-state">State</label>
						<input type="text" id="bowler-6-state" name="bowler-6-state" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-6-zip">Zip Code</label>
						<input type="text" id="bowler-6-zip" name="bowler-6-zip" value="">
					</div>
					<div class="half">
						<label for="bowler-6-average"><span class="prior-year"></span> Yearbook Average</label>
						<input type="text" id="bowler-6-average" name="bowler-6-average" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="bowler-6-pba">PBA Bowler?</label>
						<select name="bowler-6-pba" id="bowler-6-pba">
							<option value=""></option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="half">
						<label for="bowler-6-other">Select all that apply:<span>(Hold Control to select multiple options)</span></label>
						<select name="bowler-6-other" id="bowler-6-other" multiple="multiple">
							<option data-extra="year-event" value="former-champion">Former Champion</option>
							<option data-extra="bowler-role" value="state-officers">State Officers</option>
							<option value="hof">HOF</option>
							<option value="pp-iba">PP&amp;IBA Life Members</option>
							<option data-extra="bowler-role" value="usbc-board">USBC Board</option>
							<option data-extra="bowler-association" value="local-assn-managers">Local Assn Managers</option>
						</select>
					</div>
				</div>
				<div class="row extra-field year-event">
					<div class="full">
						<label for="bowler-6-former-champion-event">Former Champion Year/Event</label>
						<input type="text" class="not-required" id="bowler-6-former-champion-event" name="bowler-6-former-champion-event">
					</div>
				</div>
				<div class="row extra-field bowler-association">
					<div class="full">
						<label for="bowler-6-manager-association">Association</label>
						<input type="text" class="not-required" id="bowler-6-manager-association" name="bowler-6-manager-association">
					</div>
				</div>
				<div class="row extra-field bowler-role">
					<div class="full">
						<label for="bowler-6-role">Describe your role</label>
						<textarea class="not-required" id="bowler-6-role" name="bowler-6-role"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="full">
						<label for="bowler-6-team">Team Name</label>
						<input type="text" id="bowler-6-team" name="bowler-6-team" value="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="half">
					<a href="#" class="prev">Previous Step</a>
				</div>
				<div class="half">
					<a href="#" class="next">Next Step</a>
				</div>
			</div>
			<div class="row">
				<div class="full">
					<div class="form-errors"></div>
				</div>
			</div>

		</section>

		<section id="team-info">
			<div class="row">
				<div class="full">
					<h2>Team Information</h2>
				</div>
			</div>
			<div class="gray-back">
				<div class="row">
					<div class="full">
						<p><strong>Date &amp; Squad Preffered:</strong></p>
					</div>
					<input type="hidden" id="heading-team" name="heading-team" value="Team Information">
				</div>
				<div class="row">
					<div class="half">
						<label for="name">First choice:</label>
						<select id="squad-option-one" name="squad-option-one">
							<option value=""></option>
							<?php $count = 1;
							foreach ( get_post_meta( $post->ID, 'squads_team', true) as $option ): ?>
							<option value="<?= $option ?>"><?= $option ?> (Squad <?= $count ?>)</option>
							<?php $count++; endforeach; ?>
						</select>
					</div>
					<div class="half">
						<label for="name">Second choice:</label>
						<select id="squad-option-two" name="squad-option-two">
							<option value=""></option>
							<?php $count = 1;
							foreach ( get_post_meta( $post->ID, 'squads_team', true) as $option ): ?>
							<option value="<?= $option ?>"><?= $option ?> (Squad <?= $count ?>)</option>
							<?php $count++;
							endforeach; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<label for="team-name">Team Name</label>
						<input type="text" class="required" id="team-name" name="team-name" value="">
					</div>
					<div class="half">
						<label for="team-captain">Team Captain</label>
						<input type="text" class="required" id="team-captain" name="team-captain" value="">
					</div>
				</div>
				<div class="row">
					<div class="half">
						<p><strong>Team Registration Fee:</strong><br>$140.00</p>
					</div>
					<div class="half">
						<label for="team-optional-actual"><strong>Optional Actual</strong></label><br>
						<span class="checkbox"><input type="checkbox" id="team-optional-actual" name="team-optional-actual"> $25.00</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="half">
					<a href="#" class="prev">Previous Step</a>
				</div>
				<div class="half">
					<a href="#" class="next team">Next Step</a>
				</div>
			</div>
		</section>

		<section id="doubles">
			<div class="row">
				<div class="full">
					<h2>Singles/Doubles Squad Request</h2>
				</div>
			</div>
			<div class="gray-back">
				<div class="row">
					<div class="full">
						<p>NOTE: To Enter Doubles, you must enter Singles</p>
					</div>
				</div>
				<input type="hidden" id="heading-doubles" name="heading-doubles" value="Doubles Teams">
				<div class="row">
					<div class="full">
						<p><strong>Date &amp; Squad Preffered:</strong></p>
					</div>
					<div class="half">
						<label for="name">First choice:</label>
						<select id="squad-option-one-doubles" name="squad-option-one-doubles">
							<option value=""></option>
							<?php $count = 1;
							foreach ( get_post_meta( $post->ID, 'squads_minor', true) as $option ): ?>
							<option value="<?= $option ?>"><?= $option ?> (Squad <?= $count ?>)</option>
							<?php $count++; endforeach; ?>
						</select>
					</div>
					<div class="half">
						<label for="name">Second choice:</label>
						<select id="squad-option-two-doubles" name="squad-option-two-doubles">
							<option value=""></option>
							<?php $count = 1;
							foreach ( get_post_meta( $post->ID, 'squads_minor', true) as $option ): ?>
							<option value="<?= $option ?>"><?= $option ?> (Squad <?= $count ?>)</option>
							<?php $count++; endforeach; ?>
						</select>
					</div>
				</div>
			</div>
				
			<div class="doubles-team">
				<div class="row">
					<div class="half">
						<h3>Doubles Team 1</h3>
					</div>
					<div class="half">
						<p><strong>Optional All-Events Fees</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-1-first" id="doubles-1-first">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-1-first-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" id="doubles-1-first-all-event-handicap" name="doubles-1-first-all-event-handicap" value="3"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-1-first-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-actual" class="fee" id="doubles-1-first-all-event-actual" name="doubles-1-first-all-event-actual" value="5"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-1-second" id="doubles-1-second">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-1-second-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" id="doubles-1-second-all-event-handicap" name="doubles-1-second-all-event-handicap" value="3"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-1-second-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-actual" class="fee" id="doubles-1-second-all-event-actual" name="doubles-1-second-all-event-actual" value="5"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="full">
						<p><strong>Minors Double / Singles Fee’s</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<p>Handicap<br>$112.00</p>
					</div>
					<div class="half">
						<label for="doubles-1-minors-optional-actual">Optional Actual</label><br>
						<span class="checkbox">
							<input data-fee="optional-actual" type="checkbox" class="fee" id="doubles-1-minors-optional-actual" name="doubles-1-minors-optional-actual" value="20"> $20
						</span>
					</div>
				</div>
			</div>
			
			<div class="doubles-team">
				<div class="row">
					<div class="half">
						<h3>Doubles Team 2</h3>
					</div>
					<div class="half">
						<p><strong>Optional All-Events Fees</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-2-first" id="doubles-2-first">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-2-first-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" id="doubles-2-first-all-event-handicap" name="doubles-2-first-all-event-handicap" value="3"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-2-first-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" class="fee" id="doubles-2-first-all-event-actual" name="doubles-2-first-all-event-actual" data-fee="all-event-actual" value="5"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-2-second" id="doubles-2-second">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-2-second-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" id="doubles-2-second-all-event-handicap" name="doubles-2-second-all-event-handicap" value="3"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-2-second-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" class="fee" id="doubles-2-second-all-event-actual" name="doubles-2-second-all-event-actual"  data-fee="all-event-actual" value="5"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="full">
						<p><strong>Minors Double / Singles Fee’s</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<p>Handicap<br>112.00</p>
					</div>
					<div class="half">
						<label for="doubles-2-minors-optional-actual">Optional Actual</label><br>
						<span class="checkbox">
							<input type="checkbox" data-fee="optional-actual" class="fee" value="20" id="doubles-2-minors-optional-actual" name="doubles-2-minors-optional-actual"> $20
						</span>
					</div>
				</div>
			</div>

			<div class="doubles-team">
				<div class="row">
					<div class="half">
						<h3>Doubles Team 3</h3>
					</div>
					<div class="half">
						<p><strong>Optional All-Events Fees</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-3-first" id="doubles-3-first">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-3-first-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" value="3" id="doubles-3-first-all-event-handicap" name="doubles-3-first-all-event-handicap"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-3-first-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-actual" class="fee" value="5" id="doubles-3-first-all-event-actual" name="doubles-3-first-all-event-actual"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<select class="doubles-options" name="doubles-3-second" id="doubles-3-second">
							<option value=""></option>
						</select>
					</div>
					<div class="half">
						<div class="half">
							<label for="doubles-3-second-all-event-handicap">Handicap</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-handicap" class="fee" value="3" id="doubles-3-second-all-event-handicap" name="doubles-3-second-all-event-handicap"> $3.00
							</span>
						</div>
						<div class="half">
							<label for="doubles-3-second-all-event-actual">Actual</label><br>
							<span class="checkbox">
								<input type="checkbox" data-fee="all-event-actual" class="fee" value="5" id="doubles-3-second-all-event-actual" name="doubles-3-second-all-event-actual"> $5.00
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="full">
						<p><strong>Minors Double / Singles Fee’s</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<p>Handicap<br>$112.00</p>
					</div>
					<div class="half">
						<label for="doubles-3-minors-optional-actual">Optional Actual</label><br>
						<span class="checkbox">
							<input type="checkbox" data-fee="optional-actual" class="fee" value="20" id="doubles-3-minors-optional-actual" name="doubles-3-minors-optional-actual"> $20
						</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="half">
					<a href="#" class="prev">Previous Step</a>
				</div>
				<div class="half">
					<a href="#" class="next doubles-teams final">Next Step</a>
				</div>
			</div>
			
		</section>

		<section id="breakdown">
			<div class="row">
				<div class="full">
					<h2>Entry Summary</h2>
				</div>
			</div>
			<div class="row">
				<div class="full mb-50">
					<p class="fees"><strong>Total Team Handicap</strong> <span id="team-handicap-fees"></span></p>
					<hr>
					<p class="fees"><strong>Total Team Actual Fees</strong> <span id="team-actual-fees"></span></p>
					<hr>
					<p class="fees"><strong>Total Singles/Doubles Handicap Fees</strong> <span id="singles-doubles-handicap-fees"></span></p>
					<hr>
					<p class="fees"><strong>Total Singles/Doubles Actual Fees</strong> <span id="singles-doubles-actual-fees"></span></p>
					<hr>
					<p class="fees"><strong>Total All Events Handicap Fees</strong> <span id="total-all-event-handicap-fees"></span></p>
					<hr>
					<p class="fees"><strong>Total All Event Actual Fees</strong> <span id="total-all-event-actual-fees"></span></p>
					<hr>
					<p class="fees"><strong>Sub Total</strong> <span id="sub-total"></span></p>
					<?php if ($controller->is_late()) : ?>
						<hr>
						<p class="fees"><strong>Late Fee:</strong> <span>$25.00</span></p>
					<?php endif; ?>
					<hr>
					<p class="fees"><strong>Proccessing fee (2.5%):</strong> <span id="processing-fee"></span></p>
					<hr>
					<p class="fees"><strong>Grand Total</strong> <span id="grand-total"></span></p>
				</div>
			</div>
			<div class="row">
				<div class="full">
					<a href="#" class="btn" id="additional-bowlers-btn">Add additional teams/bowlers</a>
				</div>
			</div>
			<div class="row">
				<div class="half">
					<a href="#" class="prev">Previous Step</a>
				</div>
				<div class="half">
					<a href="#" class="btn" id="checkout-btn">Continue to payment</a>
				</div>
			</div>
		</section>

			<!-- displays form errors -->
		<div class="form-error-messages"></div>
	
		<input type="hidden" id="form-url" name="form-url" value="<?= get_permalink() ?>">
		<input type="hidden" id="submit-action" name="submit-action" value="checkout">
		<input type="hidden" id="ec-value" name="ec-value" value="">
		<input type="hidden" id="ec-processing" name="ec-processing" value="">
		<?php if ($controller->is_late()) : ?>
			<input type="hidden" id="ec-lf" name="ec-lf" value="yes">
		<?php endif; ?>

	</form>

</div>