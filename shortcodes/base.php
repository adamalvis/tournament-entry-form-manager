<?php

	class USBCShortcodeBase {
		protected $name;
		protected $template_scripts;
		protected $template_styles;

		public function register () {
			add_shortcode( $this->name, array( $this, 'load_template' ) );
		}

		public function load_template ($attrs) {

			// load static assets
			$this->load_template_scripts();
			$this->load_template_styles();

			ob_start();

			// post var passed to template
			$post = get_post($attrs['id']);
			$controller = $this;

			// save post to controller
			$this->post = $post;

			// include shortcode template
		    include(dirname(__FILE__) . '/templates/' . $this->name . '.php');

		    return ob_get_clean();
		    
		}

		public function isDev () {
			return strpos( $_SERVER['HTTP_HOST'], 'localhost' ) !== false;
		}

		public function load_template_scripts () {

			if ( $this->template_scripts !== null ) {
				foreach ( $this->template_scripts as $key => $val ) {
					$script_path = strpos($val, 'http') !== false ? $val : PLUGIN_DIR . $val;
					wp_enqueue_script( $key, $script_path );
				}
			}

		}

		public function load_template_styles () {

			if ( $this->template_styles !== null ) {
				foreach ( $this->template_styles as $key => $val ) {
					wp_enqueue_style( $key, PLUGIN_DIR . $val );
				}
			}

		}


	}