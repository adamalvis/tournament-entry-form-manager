<?php

	require_once 'base.php';

	class FormShortcode extends USBCShortcodeBase {
		private $deadline;

		public function __construct () {

			$this->name = 'form';

			// scripts to include with template relative to plugin base dir
			$this->template_scripts = array(
				'jquery' => 'https://code.jquery.com/jquery-2.2.4.min.js',
				'md5' => 'static/js/vendor/md5.min.js',
				'form-js' => 'static/js/form.js',
				// 'form-tests' => 'static/js/form-tests.js',
				'express-checkout' => 'static/js/paypal/main.js',
				'google-script' => 'http://www.paypalobjects.com/api/checkout.js',
			);

			if ( $this->isDev() ) {
				$this->template_scripts['form-tests'] = 'static/js/form-tests.js';
			}

			$this->template_styles = array(
				'form-css' => '/static/css/form.css',
			);

		}

		public function load_template ($attrs) {
			$this->deadline = strtotime(get_post_meta($attrs['id'], 'deadline', true));
			return parent::load_template($attrs);
		}

		public function organizer ($key) {
			if ( isset( $_SESSION['organizer'] ) ) {
				return $_SESSION['organizer'][$key];
			}
			return '';
		}

		public function get_prior_team_fees () {

			$total_fees = 0;

			foreach ($_SESSION['transactions'] as $fee) {
				$total_fees += $fee['total'];
			}

			return '$' . $total_fees;
			
		}

		public function get_deadline () {
			return $this->deadline;
		}

		public function is_late () {

			$override_code = $this->get_late_fee_override_code();

			// check for override
			if ( isset( $_GET['lf'] ) && $_GET['lf'] === $override_code ) {
				return false;
			}

			return time() > $this->deadline;
			
		}

		private function get_late_fee_override_code () {
			return get_post_meta($this->post->ID, 'lf-override', true);
		}

		public function local_associations () {
			return array(
				'Adams County USBC (86431)',
                'Alexandria USBC (86754)',
                'Anderson USBC (86438)',
                'Artesian USBC (81865)',
                'Attica USBC (81910)',
                'Batesville USBC (81908)',
                'Benton County USBC (81906)',
                'Bloomington USBC (81904)',
                'Bluffton USBC (86779)',
                'Brandywine USBC (81845)',
                'Chain O\' Lakes USBC (81870)',
                'City of Firsts USBC (81875)',
                'Columbus USBC (81899)',
                'Connersville Whitewater Valley USBC (84281)',
                'Crawfordsville USBC (86447)',
                'Crown City USBC (86598)',
                'Daviess-Martin USBC (81831)',
                'DeKalb County USBC (86325)',
                'Elwood USBC (86210)',
                'Fort Wayne Metro USBC (86898)',
                'Frankfort USBC (81888)',
                'Franklin USBC (81887)',
                'Goshen USBC (86552)',
                'Gr Calumet Area USBC (86391)',
                'Gr Cincinnati USBC (86669)',
                'Gr Evansville USBC (81890)',
                'Gr Greencastle USBC (81884)',
                'Gr Hamilton County USBC (81856)',
                'Gr Knox County USBC (86505)',
                'Gr La Porte County USBC (86732)',
                'Gr Terre Haute USBC (86523)',
                'Grant County USBC (86334)',
                'Greensburg USBC (81883)',
                'Hartford City USBC (81881)',
                'Heart City USBC (86322)',
                'Henry County USBC (81880)',
                'Huntingburg USBC (81843)',
                'Indy USBC (81879)',
                'Jasper USBC (81878)',
                'Kendallville USBC (81877)',
                'La Grange USBC (81873)',
                'Lafayette USBC (81874)',
                'LakeShore USBC (86387)',
                'Lebanon USBC (86196)',
                'Linton USBC (86281)',
                'Loganland USBC (81868)',
                'Louisville Metro USBC (86573)',
                'Marshall County USBC (81852)',
                'Miami County USBC (86362)',
                'Michigan City USBC (86801)',
                'Mt Vernon USBC (81860)',
                'Muncie USBC (81859)',
                'Nappanee USBC (81858)',
                'New Carlisle USBC (84241)',
                'Orange County USBC (86627)',
                'Portland USBC (86919)',
                'Princeton USBC (86763)',
                'Quad County USBC (81895)',
                'Randolph County USBC (84210)',
                'Rochester USBC (81849)',
                'Rush County USBC (86828)',
                'Scottsburg USBC (81847)',
                'Seymour USBC (81846)',
                'St Joe Valley USBC (81844)',
                'Steuben County USBC (81911)',
                'Sturgis USBC (86722)',
                'Sullivan County USBC (81842)',
                'Sycamore USBC (86925)',
                'Tell City USBC (86515)',
                'Tipton USBC (81838)',
                'Union City USBC (81837)',
                'Wabash Cannonball USBC (86917)',
                'Wakarusa USBC (81833)',
                'Warsaw USBC (86336)',
                'Wawasee USBC (81841)',
                'Wayne County USBC (86310)',
                'White County USBC (86909)',
                'Whitley County USBC (84283)',
			);
		}

	}