<?php
	get_header();
	require_once dirname(__DIR__) . '/controllers/finished.php'; 
?>
<div id="container">
	<div id="main-col">
		<div id="content">
			<?php if ( $details === false || $details['ACK'] !== 'SUCCESS' ) : ?>
				<article>
					<div class="entry-content">
						<h1>Transaction Cancelled</h1>
						<p>Your transaction was cancelled.  You have not been charged any fees. To register a team please restart the registration process.</p>
					</div>
				</article>
			<?php else: ?>
				<article>
					<div class="entry-content">
						<h1>Payment Successful</h1>
						<p><strong>Total Amount Charged:</strong> $<?= number_format($details['PAYMENTINFO_0_AMT'], 2) ?></p>
						<p><strong>Confirmation Number:</strong> <?= $details['PAYMENTINFO_0_TRANSACTIONID'] ?></p>
						<p><strong>Squad Dates/Times</strong> Dates and times are not confirmed until you get a confirmation letter from the Tournament Manager.  If Preferred Choices are not available, Date & Squad Will Be Assigned as close to the request as possible.  You will receive an e-mail from the Tournament Manager with copies of the PDF entry form(s).</p>
					</div>
				</article>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>