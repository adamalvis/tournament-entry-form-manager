<?php 
	get_header();
?>

<div id="container">
	<div id="main-col">
		<div id="content">
			<article class="page">
				<div class="entry-content" style="margin-bottom: 50px;">
					<h1>Tournament Entry Checkout</h1>

					<h3>Checkout</h3>

					<p>By clicking the PayPal Checkout button below, you will be leaving the IS USBC BA website to go to the secure PayPal site to make your secure payment.</p>

					<form action="<?= PLUGIN_DIR ?>actions/paypal/ec_call.php" method="POST" id="checkout-form">
						
					</form>
				</div>
			</article>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="http://www.paypalobjects.com/api/checkout.js"></script>
<script src="<?= PLUGIN_DIR ?>static/js/paypal/main.js"></script>

<?php get_footer(); ?>