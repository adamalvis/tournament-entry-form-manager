<?php 
	require_once dirname(__FILE__) . '/../controllers/review-order.php';
	get_header();
?>

<link rel="stylesheet" href="<?= PLUGIN_DIR ?>static/css/form.css">

<div id="container">
	<div id="main-col">
		<div id="content">
			<article class="page">
				<div class="entry-content" style="margin-bottom: 50px;">
					<div class="tournament-form">
						<h1>review order</h1>
						<div class="row">
							<!-- <div class="half">
								<h3>Your Information</h3>
								<p><strong>Name:</strong> <?= $order['name'] ?></p>
								<p><strong>Email:</strong> <?= $order['email'] ?></p>
							</div> -->

							<div class="half">
								<h3>Order Details</h3>
								<p class="cb"><strong class="fl">Entry Fees:</strong> <span class="fr">$<?= $order['before-processing'] ?></span></p>
								<?php if ($order['late-fee']): ?>
									<p class="cb"><strong class="fl">Late Fee(s):</strong> <span class="fr">$<?= $order['late-fee'] ?></span></p>
								<?php endif; ?>
								<p class="cb"><strong class="fl">2.5% Processing Fee:</strong> <span class="fr">$<?= $order['processing-fee'] ?></span></p>
								<p class="cb"><strong class="fl">Total:</strong> <span class="fr">$<?= $order['total-fees'] ?></span></p>
							</div>
						</div>

						<div class="row">
							<div class="full">
								<p>By clicking the PayPal Checkout button below, you will be leaving the IS USBC BA website to go to the secure PayPal site to make your secure payment.</p>
								<div id="paypal-button"></div>
							</div>
						</div>

					</div>
					<div class="confirmation-text" style="display: none;">
						<h1>Payment Successful</h1>
						<p><strong>Total Amount Charged:</strong> $<?= $order['total-fees'] ?></p>
						<p><strong>Confirmation Number:</strong> <span id="transaction-id"></span></p>
						<p><strong>Squad Dates/Times</strong> Dates and times are not confirmed until you get a confirmation letter from the Tournament Manager.  If Preferred Choices are not available, Date & Squad Will Be Assigned as close to the request as possible.  You will receive an e-mail from the Tournament Manager with copies of the PDF entry form(s).</p>
					</div>
					<div class="error-text" style="display: none;">
						<p>There was an issue processing your paypal. Please try again later and contact us if the problem persists.</p>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script>
	var dbUrl = '<?= PLUGIN_DIR ?>actions/finalize.php';

	paypal.Button.render({
		
		env: 'production', // Or 'sandbox'

		client: {
			sandbox:    'Aez52JmubwyukjJ3Z4kxszgH6bpucSl0oO1NrjUgAzPhAgZ4yE5gaCNDxoTqFypOiiwUlyJ_rFMwyt-E',
			production: 'AR9VmdSojP1oVAonrasD3n-D0NZAqpXSPQY6L9coYlo6R7J11nceL_QuYIk_czgctJlpSoPkh3_9JCMg'
		},

		commit: true, // Show a 'Pay Now' button

		payment: function(data, actions) {
			return actions.payment.create({
				payment: {
					transactions: [
						{
							amount: { total: '<?= $order['total-fees'] ?>', currency: 'USD' }
						}
					]
				}
			});
		},

		onAuthorize: function(data, actions) {
			console.log(data, actions);
			return actions.payment.execute().then(function(payment) {
				$('.tournament-form').hide();
				if (payment.state === 'approved') {
					$('#transaction-id').html(payment.id);
					$('.confirmation-text').show();
					console.log(payment);
					$.ajax({
						data: {
							transaction_id: payment.id,
							payment_amount: payment.transactions[0].amount.total,
						},
						method: 'POST',
						url: dbUrl,
						dataType: 'json',
					})
						.done(function (data) {
							console.log(data);
						});
				} else {
					$('.error-text').show();
				}
			});
		}

	}, '#paypal-button');
</script>

<?php get_footer(); ?>