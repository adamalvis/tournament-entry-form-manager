<?php 

	if ( ! function_exists( 'post_exists' ) ) {
	    require_once( ABSPATH . 'wp-admin/includes/post.php' );
	}

	class USBCPages {
		public static $PAGES = array(

			'review-order' => array(
				'post_title' => '(Tournament Entry Manager) Review Order',
				'post_type' => 'page',
				'post_name' => 'tournament-form-review-order',
				'post_content' => 'DO NOT REMOVE OR EDIT THIS PAGE.',
				'template' => '/templates/review-order.php',
			),

			'checkout-start' => array(
				'post_title' => '(Tournament Entry Manager) Checkout',
				'post_type' => 'page',
				'post_name' => 'tournament-checkout',
				'post_content' => 'DO NOT REMOVE OR EDIT THIS PAGE.',
				'template' => '/templates/checkout.php',
			),

			'checkout-end' => array(
				'post_title' => '(Tournament Entry Manager) Finished',
				'post_type' => 'page',
				'post_name' => 'tournament-registration-finished',
				'post_content' => 'DO NOT REMOVE OR EDIT THIS PAGE',
				'template' => '/templates/finished.php',
			),

		);

		public function create () {

			foreach (self::$PAGES as $key => $page) {

				if ( post_exists( $page['post_title'] ) === 0 ) {
					$new_page = wp_insert_post(array(
						'post_title' => $page['post_title'],
						'post_name' => $page['post_name'],
						'post_type' => $page['post_type'],
						'post_status' => 'publish',
						'post_content' => $page['post_content'],
						'post_author'    => 1,
				        'ping_status'    => 'closed',
				        'comment_status' => 'closed'
					), true);
				}

				add_filter( 'page_template', array( $this, 'set_page_templates' ) );

			}

		}

		public function set_page_templates ( $page_template ) {

			foreach( self::$PAGES as $key => $page ) {
				if ( is_page( $page['post_title'] ) ) {
					$page_template = dirname(__FILE__) . $page['template'];
				}
			}

			return $page_template;

		}

	}

	function create_usbc_pages () {
		$pages = new USBCPages();
		$pages->create();
	}

	add_action('init', 'create_usbc_pages');