<?php

	$transactions = isset( $_SESSION['transactions'] ) ? $_SESSION['transactions'] : false;
	$details = isset( $_SESSION['payment-details'] ) ? $_SESSION['payment-details'] : false;

	// check for successful payment
	if ($details['ACK'] === 'SUCCESS') {

		require_once dirname(__FILE__) . '/../../actions/mail.php';

		foreach ($transactions as $transaction) {
			update_post_meta($transaction['id'], 'payment-status', 'paid');
			update_post_meta($transaction['id'], 'paypal-transaction-id', $details['PAYMENTINFO_0_TRANSACTIONID']);
			update_post_meta($transaction['id'], 'amount-paid', $details['PAYMENTINFO_0_AMT']);
		}

		// email pdf
		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

	}
	
	// echo '<pre>'; var_dump($_SESSION); echo '</pre>';
	
	$_SESSION['transactions'] = array();