<?php

    // require_once dirname(__FILE__) . '/../../actions/paypal/ec_payer_details.php';
	require_once dirname(__FILE__) . '/../../actions/transactions/transaction-totaler.php';

	$totaler = new TransactionTotaler($_SESSION['transactions']);

	$fees = $totaler->get_total();

	$order = array(
        'before-processing' => number_format($totaler->get_sub_total(), 2),
		'processing-fee' => number_format($totaler->total_processing_fees(), 2),
        'total-fees' => number_format($fees, 2),
		'late-fee' => number_format($totaler->total_late_fees(), 2),
        'processing' => $PROCESSING_PERCENT * 100,
		// 'name' => $ec_call_data['FIRSTNAME'] . ' ' . $ec_call_data['LASTNAME'],
		// 'email' => $ec_call_data['EMAIL'],
		// 'token' => $ec_call_data['TOKEN'],
		// 'payer-id' => $ec_call_data['PAYERID'],
	);

?>