<?php

	require_once 'forms.php';
	require_once 'entries.php';

	// registers all custom post types

	function create_usbc_custom_post_types () {

		// register usbc_form post type
		$forms = new TournamentForms();
		$forms->register_post_type();

		// register usbc_entry post type
		$entries = new TournamentEntries();
		$entries->register_post_type();

	}