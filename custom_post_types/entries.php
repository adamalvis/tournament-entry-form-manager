<?php 

	require_once 'base.php';

	/*
	 *	Tournament Entries Custom Post Type Class
	 */

	class TournamentEntries extends CPTBase {

		public function __construct () {
			$this->name = 'usbc_entry';
			$this->singular_display_name = 'Entry';
			$this->plural_display_name = 'Entries';
			$this->icon = 'dashicons-id';
			$this->supports = array( 'title' );
			$this->capabilities = array(
				'edit_post'          => 'edit_usbc_entry', 
				'read_post'          => 'read_usbc_entry', 
				'delete_posts'       => 'delete_usbc_entry', 
				'edit_posts'         => 'edit_usbc_entry', 
				'edit_others_posts'  => 'edit_others_usbc_entry', 
				'publish_posts'      => 'publish_usbc_entry',       
				'read_private_posts' => 'read_private_usbc_entry', 
				'create_posts'       => 'edit_usbc_entry', 
			);
		}
	}