<?php

	require_once 'base.php';

	/*
	 *	Tournament Forms Custom Post Type Class
	 */

	class TournamentForms extends CPTBase {
		
		public function __construct () {

			$this->name = 'usbc_forms';
			$this->singular_display_name = 'Form';
			$this->plural_display_name = 'Forms';
			$this->icon = 'dashicons-feedback';
			$this->supports = array( 'title', 'editor' );
			$this->capabilities = array(
				'edit_post'          => 'edit_usbc_forms', 
				'read_post'          => 'read_usbc_forms', 
				'delete_posts'       => 'delete_usbc_forms', 
				'edit_posts'         => 'edit_usbc_forms', 
				'edit_others_posts'  => 'edit_others_usbc_forms', 
				'publish_posts'      => 'publish_usbc_forms',       
				'read_private_posts' => 'read_private_usbc_forms', 
				'create_posts'       => 'edit_usbc_forms',
			);

		}

		protected function create_shortcode_meta_box () {
			add_action( 'add_meta_boxes', array( $this, 'add_post_meta_boxes' ) );
		}

		public function add_post_meta_boxes ( $post_id ) {
			add_meta_box( 
			    'form-shortcode-box',
			    __( 'Form Shortcode' ),
			    array( $this, 'create_shortcode_metabox_html' ),
			    $this->name,
			    'side',
			    'default'
			);
			add_meta_box( 
			    'form-lf-override-box',
			    __( 'Late Fee Override Link' ),
			    array( $this, 'create_lf_override_metabox_html' ),
			    $this->name,
			    'side',
			    'default'
			);
		}

		public function create_shortcode_metabox_html () {
			global $post;
			echo '<input type="text" value="[form id=' . $post->ID . ']" />';
		}

		public function create_lf_override_metabox_html () {

			global $post;

			$form_page = $this->get_form_page();
			$override_code = $this->get_override_code();

			if ($form_page) {
				echo '<input type="text" value="' . get_permalink( $form_page ) .'?lf=' . $override_code . '"/>';
				echo '<p>Using the above link will prevent the user from being charged a late fee.</p>';
			} else {
				echo '<p>Add this form to a page to create an override link</p>';
			}		

		}

		private function get_override_code () {

			global $post;

			$override_code = get_post_meta($post->ID, 'lf-override', true);

			if ( $override_code === '' || $override_code === null) {
				$new_code = uniqid('oc_');
				add_post_meta( $post->ID, 'lf-override', $new_code, true );
				$override_code = $new_code;
			}

			return $override_code;

		}

		private function get_form_page () {

			global $post;

			$form_page = null;

			$pages = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => 'page',
			));

			$form_code = '[form id=' . $post->ID . ']';

			foreach ($pages as $page ) {
				if ( strpos($page->post_content, $form_code ) !== false ) {
					$form_page = $page;
				}
			}

			return $form_page;

		}
		
	}