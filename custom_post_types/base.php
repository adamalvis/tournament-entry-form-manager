<?php
	
	/*
	 *	BASE Custom Post Type class
	 */


	class CPTBase {
		protected $name;
		protected $plural_display_name;
		protected $singular_display_name;
		protected $supports;
		protected $icon;

		public function register_post_type () {
			add_action( 'init', array( $this, 'create_post_type' ) );
		}

		public function create_post_type () {
			register_post_type( $this->name,
			  	array(
		            'labels' => array(
		                'name' => $this->plural_display_name,
		                'singular_name' => $this->singular_display_name,
		                'add_new' => 'Add New',
		                'add_new_item' => 'Add New ' . $this->singular_display_name,
		                'edit' => 'Edit',
		                'edit_item' => 'Edit ' . $this->singular_display_name,
		                'new_item' => 'New ' . $this->singular_display_name,
		                'view' => 'View',
		                'view_item' => 'View ' . $this->singular_display_name,
		                'search_items' => 'Search ' . $this->plural_display_name,
		                'not_found' => 'No ' . $this->plural_display_name . ' found',
		                'not_found_in_trash' => 'No ' . $this->plural_display_name . ' found in Trash',
		            ),
		 
		            'public' => true,
		            'menu_position' => 15,
		            'supports' => $this->supports,
		            'taxonomies' => array( '' ),
		            'menu_icon' => $this->icon,
		            'has_archive' => true,
		            // 'capabilities' => $this->capabilities,
		        )
			);

			// create metaboxes if set
			if ( method_exists( $this, 'create_shortcode_meta_box' ) ) {
				$this->create_shortcode_meta_box();
			}
		}

	}