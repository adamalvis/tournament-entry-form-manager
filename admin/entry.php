<?php

	require_once 'base.php';

	class EntryAdmin extends AdminBase {
		public $EXCLUDED_FIELDS = array(
			'_edit_lock',
			'read-rules',
			'pdf',
			'ec-value',
			'ec-processing',
			'submit-action',
			'form-url',
		);

		public function __construct () {

			$this->post_type = 'usbc_entry';
			$this->styles = array(
				'entry-admin-css' => 'static/css/entry-admin.css',
			);

			$this->scripts = array(
				'entry-admin-js' => 'static/js/entry-admin.js',
			);

			// convert string search to post id query
			add_filter( 'parse_query', array($this, 'custom_entry_query') );

			// add new columns
			add_filter('manage_posts_columns', array($this, 'create_custom_column'));
			add_action('manage_posts_custom_column', array($this, 'add_custom_column_data'), 10, 2);

			// make columns sortable
			add_action('manage_edit-usbc_entry_sortable_columns', array($this, 'make_columns_sortable'));
			add_action('pre_get_posts', array($this, 'set_orderby'));

			parent::__construct();

		}

		public function format_label ($label) {
			$count = 100;
			return str_replace('-', ' ', $label, $count);
		}

		public function is_heading ($field) {
			return strpos($field, 'heading') !== false;
		}

		public function render_template ($post, $box) {
			$instance = $this;
			include( dirname(__FILE__) . '/templates/entry.php' );
		}

		public function save_form ($post_id) {
			// nothing
		}

		public function get_pdf ($post_id) {
			$file = get_post_meta($post_id, 'pdf', true);
			return PLUGIN_DIR . 'pdfs/' . $file;
		}

		public function custom_entry_query ($query) {
			$query->query_vars['s'] = '';
			$query->query_vars['page_id'] = intval($_GET['s']);;
			// echo '<pre>'; var_dump($query); echo '</pre>';
		}

		public function create_custom_column ($defaults) {
			$defaults['payment_status'] = 'Payment Status';
			$defaults['entry_id'] = 'Entry ID';
			return $defaults;
		}

		public function add_custom_column_data ( $column_name, $post_id ) {
			if ( $column_name === 'payment_status' ) {
				echo get_post_meta($post_id, 'payment-status', true);
			} else if ($column_name === 'entry_id' ) {
				echo $post_id;
			}
		}

		public function make_columns_sortable ( $columns ) {
			$columns['payment_status'] = 'payment';
			$columns['entry_id'] = 'entry';
			return $columns;
		}

		public function set_orderby ( $query ) {
			if ( ! is_admin() )
				return;
			
			$orderby = $query->get('orderby');

			if ( $orderby === 'payment' ) {
				$query->set('meta_key', 'payment-status');
				$query->set('orderby', 'meta_value');
			}
		}

	}