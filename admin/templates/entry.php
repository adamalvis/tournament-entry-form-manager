<div class="entry-admin">
	<div class="row">
		<div class="full">
			<div class="download-pdf">
				<a class="button-primary" download href="<?= $instance->get_pdf($_GET['post']) ?>" target="_blank">Download PDF</a>
			</div>
		</div>
	</div>
	<div class="row">
		<?php foreach ( get_post_meta( $_GET['post'] ) as $key => $val ) : ?>
			<?php if ( ! in_array( $key, $instance->EXCLUDED_FIELDS ) ) : ?>
				
				<?php if ($instance->is_heading( $key ) ) : ?>
					<div class="full">
						<h1><?= $val[0] ?></h1>
					</div>
				<?php else: ?>
					<div class="half">
						<label for="<?= $key ?>"><?= $instance->format_label($key) ?></label>
						<input type="text" id="<?= $key ?>" name="<?= $key ?>" value="<?= $val[0] ?>">
					</div>
				<?php endif; ?>

			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div>