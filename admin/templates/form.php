<?php 
	$squads_team = get_post_meta( $post->ID, 'squads_team', true) ? get_post_meta( $post->ID, 'squads_team', true) : [''];
	$squads_minor = get_post_meta( $post->ID, 'squads_minor', true) ? get_post_meta( $post->ID, 'squads_minor', true) : [''];
	$deadline = get_post_meta( $post->ID, 'deadline', true) ? get_post_meta( $post->ID, 'deadline', true) : ''; 
?>

<div class="form-admin">

	<p><strong>Squads Team</strong></p>
	<div class="squad-times" id="squads-team-wrapper">
		<?php foreach ( $squads_team as $squad ): ?>
			<input class="datetime" type="text" value="<?= $squad ?>" name="squads_team[]" id="squads_team[]">
		<?php endforeach; ?>
	</div>
	<input type="button" class="button button-primary" value="Add time" id="add-squad-team" name="add-squad-team">

	<p><strong>Squads Minor</strong></p>
	<div class="squad-times" id="squads-minor-wrapper">
		<?php foreach ( $squads_minor as $squad ): ?>
			<input class="datetime" type="text" value="<?= $squad ?>" name="squads_minor[]" id="squads_minor[]">
		<?php endforeach; ?>
	</div>
	<input type="button" class="button button-primary" value="Add time" id="add-squad-minor" name="add-squad-minor">

	<p><strong>Entry Deadline:</strong></p>
	<input type="text" class="datetime" value="<?= $deadline ?>" name="deadline" id="deadline">

</div>