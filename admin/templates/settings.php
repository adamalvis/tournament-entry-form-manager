<div class="wrap">
    <h1>Tournament Entry Manager Settings</h1>
    <p>Click below to export a csv of all entries</p>
    <a href="<?= $controller->get_csv_file_location() ?>" download target="_blank" class="button button-primary">Download Entry CSV</a>
</div>