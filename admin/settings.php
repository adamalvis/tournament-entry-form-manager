<?php

class USBCBASettings {
    private $csv_folder = PLUGIN_DIR . 'csv';

    public function __construct() {
        add_action( 'admin_menu', array( $this, 'add_usbcba_menu' ) );
    }

	public function add_usbcba_menu() {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'Tournament Entry Manager', 
            'manage_options', 
            'tournament-entry-manager-settings', 
            array( $this, 'create_admin_page' )
        );
    }

    public function create_admin_page() {
        ob_start();
        $controller = $this;
        include('templates/settings.php');
        echo ob_get_clean();
    }

    public function csv() {
        $entries = get_posts(array(
            'post_type' => 'usbc_entry',
            'posts_per_page' => -1,
        ));
        $headers = $this->get_csv_headers($entries[0]);
        $csv_formatted_entries = array($headers);
        foreach ( $entries as $entry ) {
            $ordered_entry_data = array();
            $entry_data = get_post_meta($entry->ID);
            $entry_data['entry_id'] = $entry->ID;

            // order fields
            foreach( $headers as $key ) {
                $data = isset( $entry_data[$key] ) ? $entry_data[$key] : '';
                $ordered_entry_data[$key] = $data;
            }

            $entry_data = $this->format_entry_data($ordered_entry_data);
            array_push($csv_formatted_entries, $entry_data);
            // var_dump($entry_data);
        }
        return $csv_formatted_entries;
    }

    public function get_csv_file_location() {
        $csv_array = $this->csv();
        $csv_file = fopen(dirname(__FILE__) . '/../csv/tournament-entries.csv', 'w');
        foreach ($csv_array as $fields) {
            fputcsv($csv_file, $fields);
        }
        return $this->csv_folder . '/tournament-entries.csv';
    }

    private function format_entry_data($data) {
        $formatted_data = array();
        foreach ( $data as $name => $field ) {
            if ( is_array( $field ) ) {
                array_push($formatted_data, $field[0]);
            } else {
                array_push($formatted_data, $field);
            }
        }
        return $formatted_data;
    }

    private function get_csv_headers($entry) {
        $headers = array();
        foreach ( get_post_meta($entry->ID) as $name => $field ) {
            if ( strpos($name, 'heading-') === false ) {
                array_push($headers, $name);
            }
        }
        array_push($headers, 'entry_id');
        return $headers;
    }
}
