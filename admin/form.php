<?php

	require_once 'base.php';

	class FormAdmin extends AdminBase {

		public function __construct () {

			$this->post_type = 'usbc_forms';
			$this->scripts = array(
				'moment-js' => 'static/vendor/datetimepicker/moment.js',
				'datetime-js' => 'static/vendor/datetimepicker/datetimepicker.js',
				'form-admin-js' => 'static/js/form-admin.js',
			);
			$this->styles = array(
				'form-admin-css' => 'static/css/form-admin.css',
				'datetime-css' => 'static/vendor/datetimepicker/datetimepicker.css',
			);

			parent::__construct();
		}

		public function render_template ($post, $box) {
			include( dirname(__FILE__) . '/templates/form.php' );
		}

		public function save_form ( $post_id ) {

			$squads_team = $this->remove_empty_items( $_POST['squads_team'] );

			if ( isset( $squads_team ) ) {
				if ( ! add_post_meta( $post_id, 'squads_team', $squads_team, true ) ) {
					update_post_meta(
						$post_id,
						'squads_team',
						$squads_team
					);
				}
			}

			$squads_minor = $this->remove_empty_items( $_POST['squads_minor'] );

			if ( isset( $squads_minor ) ) {
				if ( ! add_post_meta( $post_id, 'squads_minor', $squads_minor, true ) ) {
					update_post_meta(
						$post_id,
						'squads_minor',
						$squads_minor
					);
				}
			}

			if ( isset( $_POST['deadline'] ) ) {
				if ( ! add_post_meta( $post_id, 'deadline', $_POST['deadline'], true ) ) {
					update_post_meta(
						$post_id,
						'deadline',
						$_POST['deadline']
					);
				}
			}

		}

		private function remove_empty_items ( $arr ) {
			for ( $i = 0; $i < count($arr); $i++ ) {
				if ( $arr[$i] === '' || $arr[$i] === null ) {
					unset( $arr[$i] );
				}
			}
			return $arr;
		}

	}