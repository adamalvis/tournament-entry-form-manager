<?php

	class AdminBase {
		protected $scripts;
		protected $styles;

		public function __construct () {

			if ( $this->in_post_admin() ) {
				add_action( 'admin_enqueue_scripts', array( $this, 'load_scripts' ) );
				add_action( 'admin_enqueue_scripts', array( $this, 'load_styles' ) );
				add_action( 'add_meta_boxes', array( $this, 'add_post_meta_boxes' ) );
			}

			add_action( 'save_post', array( $this, 'save_form' ) );

		}

		private function in_post_admin () {

			$post_type = false;

			if ( isset( $_GET['post'] ) ) {
				$post_type = get_post( $_GET['post'] )->post_type;
			}

			if ( isset( $_GET['post_type'] ) ) {
				$post_type = $_GET['post_type'];
			}

			if ( is_admin() && $post_type === $this->post_type ) {
				return true;
			}

			return false;

		}

		public function load_scripts () {

			if ( is_array( $this->scripts ) ) {
				foreach ( $this->scripts as $key => $val ) {
					wp_enqueue_script( $key, PLUGIN_DIR . $val );
				}
			}

		}

		public function load_styles () {

			if ( is_array( $this->styles ) ) {
				foreach ( $this->styles as $key => $val ) {
					wp_enqueue_style( $key, PLUGIN_DIR . $val );
				}
			}

		}

		public function add_post_meta_boxes ( $post_id ) {
			add_meta_box( 
			    $post_id,
			    $this->get_box_title(),
			    array( $this, 'render_template' ),
			    $this->post_type,
			    'normal',
			    'default'
			);

		}

		public function get_box_title () {
			return str_replace('Admin', '', get_class( $this ) );
		}

	}