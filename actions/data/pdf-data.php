<?php

    require_once dirname(__FILE__) . '/..//helpers/Fee_Calculator.php';
    require_once dirname(__FILE__) . '/..//helpers/Bowler_Types.php';


    $calculator = new Fee_Calculator($_POST);
    $bowler_types = new Bowler_Types($_POST);


	$pdf_data = array(

        // team times
        array(
            'data' => $_POST['squad-option-one'],
            'x' => 48,
            'y' => 27,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['squad-option-two'],
            'x' => 128,
            'y' => 27,
            'type' => 'text',
        ),

        // team captain / squad organizer
        array(
            'data' => $_POST['full-name'],
            'x' => 48,
            'y' => 34,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['reservation-number'],
            'x' => 164,
            'y' => 34,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['address'],
            'x' => 48,
            'y' => 40,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['evening-phone'],
            'x' => 164,
            'y' => 40,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['city'] . ', ' . $_POST['state'] . ', ' . $_POST['zip-code'],
            'x' => 48,
            'y' => 48,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['daytime-phone'],
            'x' => 164,
            'y' => 48,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['email'],
            'x' => 48,
            'y' => 55,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['local-association'],
            'x' => 164,
            'y' => 55,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['team-name'],
            'x' => 68,
            'y' => 62,
            'type' => 'text',
        ),

        // Confirmation number (WP Post ID)
        array(
            'data' => 'Online Registration #: ' . $_POST['postid'],
            'x' => 144,
            'y' => 62,
            'type' => 'text',
        ),


        // bowler #1
        array(
            'data' => $_POST['bowler-1-national-id'],
            'x' => 38,
            'y' => 82,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-1-email'],
            'x' => 38,
            'y' => 88,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-1-name'],
            'x' => 58,
            'y' => 82,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-1-address'],
            'x' => 94,
            'y' => 82,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-1-city'] . ', ' . $_POST['bowler-1-state'] . ', ' . $_POST['bowler-1-zip'],
            'x' => 94,
            'y' => 88,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-1-average'],
            'x' => 175,
            'y' => 85,
            'type' => 'text',
        ),

        // bowler #2
        array(
            'data' => $_POST['bowler-2-national-id'],
            'x' => 38,
            'y' => 94,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-2-email'],
            'x' => 38,
            'y' => 100,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-2-name'],
            'x' => 58,
            'y' => 94,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-2-address'],
            'x' => 94,
            'y' => 94,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-2-city'] . ', ' . $_POST['bowler-2-state'] . ', ' . $_POST['bowler-2-zip'],
            'x' => 94,
            'y' => 100,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-2-average'],
            'x' => 175,
            'y' => 97,
            'type' => 'text',
        ),

        // bowler #3
        array(
            'data' => $_POST['bowler-3-national-id'],
            'x' => 38,
            'y' => 106,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-3-email'],
            'x' => 38,
            'y' => 112,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-3-name'],
            'x' => 58,
            'y' => 106,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-3-address'],
            'x' => 94,
            'y' => 106,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-3-city'] . ', ' . $_POST['bowler-3-state'] . ', ' . $_POST['bowler-3-zip'],
            'x' => 94,
            'y' => 112,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-3-average'],
            'x' => 175,
            'y' => 109,
            'type' => 'text',
        ),

        // bowler #4
        array(
            'data' => $_POST['bowler-4-national-id'],
            'x' => 38,
            'y' => 118,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-4-email'],
            'x' => 38,
            'y' => 124,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-4-name'],
            'x' => 58,
            'y' => 118,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-4-address'],
            'x' => 94,
            'y' => 118,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-4-city'] . ', ' . $_POST['bowler-4-state'] . ', ' . $_POST['bowler-4-zip'],
            'x' => 94,
            'y' => 124,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-4-average'],
            'x' => 175,
            'y' => 122,
            'type' => 'text',
        ),

        // bowler #5
        array(
            'data' => $_POST['bowler-5-national-id'],
            'x' => 38,
            'y' => 130,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-5-email'],
            'x' => 38,
            'y' => 136,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-5-name'],
            'x' => 58,
            'y' => 130,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-5-address'],
            'x' => 94,
            'y' => 130,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-5-city'] . ', ' . $_POST['bowler-5-state'] . ', ' . $_POST['bowler-5-zip'],
            'x' => 94,
            'y' => 136,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-5-average'],
            'x' => 175,
            'y' => 134,
            'type' => 'text',
        ),

        // bowler #6
        array(
            'data' => $_POST['bowler-6-national-id'],
            'x' => 38,
            'y' => 146,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-email'],
            'x' => 38,
            'y' => 152,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-name'],
            'x' => 58,
            'y' => 146,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-address'],
            'x' => 94,
            'y' => 146,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-city'] . ', ' . $_POST['bowler-6-state'] . ', ' . $_POST['bowler-6-zip'],
            'x' => 94,
            'y' => 152,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-average'],
            'x' => 175,
            'y' => 149,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['bowler-6-team'],
            'x' => 78,
            'y' => 141.5,
            'type' => 'text',
        ),

        // squad options
        array(
            'data' => $_POST['squad-option-one-doubles'],
            'x' => 52,
            'y' => 165,
            'type' => 'text',
        ),
        array(
            'data' => $_POST['squad-option-two-doubles'],
            'x' => 140,
            'y' => 165,
            'type' => 'text',
        ),

        // doubles team #1
        array(
            'data' => $_POST['doubles-1-first'],
            'x' => 40,
            'y' => 186,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-1-first-all-event-handicap'] ) ? $_POST['doubles-1-first-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 188,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-1-first-all-event-actual'] ) ? $_POST['doubles-1-first-all-event-actual']: '',
            'x' => 104.5,
            'y' => 188,
            'type' => 'check',
        ),
        array(
            'data' => $_POST['doubles-1-second'],
            'x' => 40,
            'y' => 191,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-1-second-all-event-handicap'] ) ? $_POST['doubles-1-second-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 193,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-1-second-all-event-actual'] ) ? $_POST['doubles-1-second-all-event-actual'] : '',
            'x' => 104.5,
            'y' => 193,
            'type' => 'check',
        ),

        // doubles team #2
        array(
            'data' => $_POST['doubles-2-first'],
            'x' => 40,
            'y' => 196,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-2-first-all-event-handicap'] ) ? $_POST['doubles-2-first-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 198,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-2-first-all-event-actual'] ) ? $_POST['doubles-2-first-all-event-actual'] : '',
            'x' => 104.5,
            'y' => 198,
            'type' => 'check',
        ),
        array(
            'data' => $_POST['doubles-2-second'],
            'x' => 40,
            'y' => 201,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-2-second-all-event-handicap'] ) ? $_POST['doubles-2-second-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 203,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-2-second-all-event-actual'] ) ? $_POST['doubles-2-second-all-event-actual'] : '',
            'x' => 104.5,
            'y' => 203,
            'type' => 'check',
        ),

        // doubles team #3
        array(
            'data' => $_POST['doubles-3-first'],
            'x' => 40,
            'y' => 206,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-3-first-all-event-handicap'] ) ? $_POST['doubles-3-first-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 207.5,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-3-first-all-event-actual'] ) ? $_POST['doubles-3-first-all-event-actual'] : '',
            'x' => 104.5,
            'y' => 207.5,
            'type' => 'check',
        ),
        array(
            'data' => $_POST['doubles-3-second'],
            'x' => 40,
            'y' => 211,
            'type' => 'text',
        ),
        array(
            'data' => isset( $_POST['doubles-3-second-all-event-handicap'] ) ? $_POST['doubles-3-second-all-event-handicap'] : '',
            'x' => 85.5,
            'y' => 212.5,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-3-second-all-event-actual'] ) ? $_POST['doubles-3-second-all-event-actual'] : '',
            'x' => 104.5,
            'y' => 212.5,
            'type' => 'check',
        ),

        // doubles optional totals
        array(
            'data' => $calculator->doubles_optional_handicap_total(),
            'x' => 88.5,
            'y' => 216.5,
            'type' => 'dollar',
        ),
        array(
            'data' => $calculator->doubles_optional_actual_total(),
            'x' => 104.5,
            'y' => 216.5,
            'type' => 'dollar',
        ),

        // doubles team handicaps
        array(
            'data' => $calculator->doubles_team_handicap(1),
            'x' => 129.2,
            'y' => 188.5,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->doubles_team_handicap(2),
            'x' => 129.2,
            'y' => 198.5,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->doubles_team_handicap(3),
            'x' => 129.2,
            'y' => 208.2,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->minors_handicap(),
            'x' => 125,
            'y' => 216.2,
            'type' => 'dollar',
        ),

        // doubles team optional actuals
        array(
            'data' => isset( $_POST['doubles-1-minors-optional-actual'] ) ? $_POST['doubles-1-minors-optional-actual'] : '',
            'x' => 150,
            'y' => 188.5,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-2-minors-optional-actual'] ) ? $_POST['doubles-2-minors-optional-actual'] : '',
            'x' => 150,
            'y' => 198.5,
            'type' => 'check',
        ),
        array(
            'data' => isset( $_POST['doubles-3-minors-optional-actual'] ) ? $_POST['doubles-3-minors-optional-actual'] : '',
            'x' => 150,
            'y' => 208.2,
            'type' => 'check',
        ),
        array(
            'data' =>  isset( $_POST['ec-lf'] ) ? $_POST['ec-lf'] : '',
            'x' => 155,
            'y' => 225,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->minors_optional_actual(),
            'x' => 146,
            'y' => 216.2,
            'type' => 'dollar',
        ),

        // full team fees
        array(
            'data' => $_POST['team-name'],
            'x' => 169.8,
            'y' => 197.6,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->team_fee(),
            'x' => 166,
            'y' => 216.2,
            'type' => 'dollar',
        ),
        array(
            'data' => isset( $_POST['team-optional-actual'] ) ? $_POST['team-optional-actual'] : '',
            'x' => 188,
            'y' => 197.6,
            'type' => 'check',
        ),
        array(
            'data' => $calculator->team_optional_actual(),
            'x' => 186,
            'y' => 216.2,
            'type' => 'dollar',
        ),

        // total fees
        array(
            'data' => $calculator->total_fees(),
            'x' => 177,
            'y' => 221.7,
            'type' => 'dollar',
        ),

        // pba bowlers
        array(
            'data' => $bowler_types->pba_bowlers(),
            'x' => 49,
            'y' => 228.7,
            'type' => 'text',
        ),
        array(
            'data' => $bowler_types->other_types(),
            'x' => 21,
            'y' => 239.2,
            'type' => 'textarea',
        ),


    );