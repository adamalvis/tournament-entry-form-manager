<?php 

	// expose WP functions
	require_once dirname(__FILE__) . '/../../../../../wp-blog-header.php';
	require_once dirname(__FILE__) . '/../transactions/transaction-totaler.php';

	require_once 'constants.php';
	require_once 'paypal.php';

	$totaler = new TransactionTotaler($_SESSION['transactions']);

	$total_price = $totaler->get_total();

	$return_url = site_url() . '/' . USBCPages::$PAGES['checkout-end']['post_name'];

	$start_checkout = new PaypalAPI($user, $password, $signature, $return_url);

	$ec_data = $start_checkout->start_call(array(
		'USER' => $user,
		'PWD' => $password,
		'SIGNATURE' => $signature,
		'METHOD' => 'DoExpressCheckoutPayment',
		'VERSION' => 124.0,
		'TOKEN' => $_REQUEST['token'],
		'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
		'PAYERID' => $_REQUEST['PayerID'],
		'PAYMENTREQUEST_0_AMT' => $total_price,
		'PAYMENTREQUEST_0_SHIPPINGAMT' => 0,
		'PAYMENTREQUEST_0_TAXAMT' => 0 ,
		'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
		'PAYMENTREQUEST_0_DESC' => 'Tournament Entry Payment',
	));

	$_SESSION['payment-details'] = $ec_data;

	$_SESSION['redirect_url'] = $return_url;


	header('Location: ' . site_url() . '/wp-content/plugins/tournament-entry-manager/actions/finished-redirect.php');

