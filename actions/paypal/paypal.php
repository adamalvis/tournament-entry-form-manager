<?php

	class PaypalAPI {
		private $url = "https://api-3t.sandbox.paypal.com/nvp";
		private $user;
		private $password;
		private $signature;
		private $return_url;

		public function __construct ($user, $password, $signature, $return_url) {

			$this->user = $user;
			$this->password = $password;
			$this->signature = $signature;
			$this->return_url = $return_url;

		}

		public function start_call ($params) {

			if(isset($_REQUEST['token'])) {
				$token = $_REQUEST['token'];
			}

			//If there's no token call SetEC
			if(!isset($token)) {  

			    $response = $this->run_call($params); 
				$token = $response['TOKEN'];
				$paypal_url = 'https://www.sandbox.paypal.com/checkoutnow?token=' . $token;

				header("Location:" . $paypal_url);

			} else {
				return $this->run_call($params);
			}

		}

		private function run_call($nvps){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 45);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSLVERSION, 6);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($nvps));
			 
			$result = curl_exec($ch);

			$httpResponseAr = explode("&", strtoupper($result));
			$httpParsedResponseAr = array();

			foreach ($httpResponseAr as $value) {
			    $tmpAr = explode("=", $value);
			    if(sizeof($tmpAr) > 1) {
			        $httpParsedResponseAr[$tmpAr[0]] = urldecode($tmpAr[1]);
			    }
			}

			curl_close ($ch); 

			return $httpParsedResponseAr;

		}

	}