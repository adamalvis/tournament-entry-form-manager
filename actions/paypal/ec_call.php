<?php

	// expose WP functions
	require_once dirname(__FILE__) . '/../../../../../wp-blog-header.php';

	require_once dirname(__FILE__) . '/../../pages/index.php';
	require_once dirname(__FILE__) . '/../transactions/transaction-totaler.php';
	require_once 'constants.php';
	require_once 'paypal.php';

	$totaler = new TransactionTotaler($_SESSION['transactions']);

	$total_price = $totaler->get_total();

	// var_dump($total_price);

	$return_url = site_url() . '/' . USBCPages::$PAGES['review-order']['post_name'];
	$cancel_url = site_url() . '/' . USBCPages::$PAGES['checkout-end']['post_name'];

	$start_checkout = new PaypalAPI($user, $password, $signature, $return_url);
	$start_checkout->start_call(array(
		'USER' => $user,
		'PWD' => $password,
		'SIGNATURE' => $signature,
		'METHOD' => 'SetExpressCheckout',
		'VERSION' => '124.0',
		'RETURNURL' => $return_url,
		'CANCELURL' => $cancel_url,
		'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale ',
		'PAYMENTREQUEST_0_AMT' => $total_price,
		'PAYMENTREQUEST_0_CURRENCYCODE' => 'USD',
		'PAYMENTREQUEST_0_DESC' => 'Tournament Entry Payment',
		// 'SOLUTIONTYPE' => 'Sole',
	));

?>
