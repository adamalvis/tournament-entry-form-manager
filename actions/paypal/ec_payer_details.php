<?php

	// incldue WP functions
	require_once dirname(__FILE__) . '/../../../../../wp-blog-header.php';

	require_once dirname(__FILE__) . '/../../pages/index.php';
	require_once 'constants.php';
	require_once 'paypal.php';


	$PROCESSING_PERCENT = $_SESSION['processing-fee'];


	$return_url = site_url() . '/' . USBCPages::$PAGES['review-order']['post_name'];

	$start_checkout = new PaypalAPI($user, $password, $signature, $return_url);
	$ec_call_data = $start_checkout->start_call(array(
		'USER' => $user,
		'PWD' => $password,
		'SIGNATURE' => $signature,
		'METHOD' => 'GetExpressCheckoutDetails',
		'VERSION' => 124.0,
		'TOKEN' => $_GET['token'],
	));

?>