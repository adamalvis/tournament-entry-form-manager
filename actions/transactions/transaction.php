<?php

class Transaction {
    public $processing_fee_percent = 0.025;
    private $late_fee_amnt = 25;
    private $has_late_fee = false;
    private $id;
    private $total;
    private $pdf;

    public function __construct ($trans_arr) {
        $this->id = $trans_arr['id'];
        $this->total = intval($trans_arr['total']);
        $this->pdf = $trans_arr['pdf'];
        $this->has_late_fee = $trans_arr['late_fee'];
    }
    
    public function sub_total () {
        return $this->total;
    }

    public function total () {
        return $this->total + $this->get_late_fee() + $this->get_processing_fee();
    }

    public function get_late_fee () {
        return $this->has_late_fee ? $this->late_fee_amnt : 0;
    }

    public function get_processing_fee () {
        return round(($this->total + $this->get_late_fee()) * $this->processing_fee_percent, 2);
    }
}