<?php

require_once 'transaction.php';

class TransactionTotaler {
    private $transactions;

    /**
     *  @param $transactios: an array of transactions data
     */
    public function __construct ($transactions) {
        $this->transactions = array();
        foreach($transactions as $transaction) {
            array_push($this->transactions, new Transaction($transaction));
        }
    }

    public function get_total () {
        $total = 0;
        foreach ($this->transactions as $transaction) {
            $total += $transaction->total();
        }
        return $total;
    }

    public function get_sub_total () {
        $total = 0;
        foreach ($this->transactions as $transaction) {
            $total += $transaction->sub_total();
        }
        return $total;
    }

    public function total_processing_fees () {
        $total = 0;
        foreach ($this->transactions as $transaction) {
            $total += $transaction->get_processing_fee();
        }
        return $total;
    }

    public function total_late_fees () {
        $total = 0;
        foreach ($this->transactions as $transaction) {
            $total += $transaction->get_late_fee();
        }
        return $total;
    }

    public function get_transactions () {
        return $this->transactions;
    }
}