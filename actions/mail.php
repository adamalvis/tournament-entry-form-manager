<?php

	require_once dirname(__FILE__) . '/../vendor/PHPMailer/PHPMailerAutoload.php';
	require_once dirname(__FILE__) . '/../pages/controllers/review-order.php';
	require_once dirname(__FILE__) . '/../actions/transactions/transaction.php';

	$mail = new PHPMailer;

	// $mail->SMTPDebug = 3;                              // Enable verbose debug output

	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'webentry@indianastateusbc.org';                 // SMTP username
	$mail->Password = 'Zr2X>e>&2';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                 // TCP port to connect to

	$mail->setFrom('rick.schultz@indianastateusbc.org', 'Rick Schultz');
	$mail->addAddress('rick.schultz@indianastateusbc.org');
	// $mail->addAddress('adam.alvis@gmail.com', 'Adam Alvis');
	$mail->addAddress($_SESSION['organizer']['email']);
	$mail->addReplyTo('rick.schultz@indianastateusbc.org', 'Rick Shultz');
	// $mail->addCC('cc@example.com');
	// $mail->addBCC('bcc@example.com');

	$mail->isHTML(true);                                  // Set email format to HTML

	// create pdf links
	$entry_data = '';
	$count = 1;

	foreach( $_SESSION['transactions'] as $trans_arr ) {
		$transaction = new Transaction($trans_arr);
		$entry_data .=  '<p><strong>Entry #' . $count . '</strong></p>' .
						'<p>Total fees: $' . number_format($transaction->sub_total(), 2) . '</p>' .
						'<p>Late fee(s): $' . number_format($transaction->get_late_fee(), 2) . '</p>' .
						'<p>Processing fee: $' . number_format($transaction->get_processing_fee(), 2) . '</p>' .  
						'<p>Entry total: $' . number_format($transaction->total(), 2) . '</p>' . 
						'<p>Entry PDF: <a href="' . site_url() . '/wp-content/plugins/tournament-entry-manager/pdfs/' . $trans_arr['pdf'] . '">View Details</a></p>';
		$count++;
	}

	$mail->Subject = 'Open Championship Entry Confirmation';
	$mail->Body    = 'Entry Confirmation for ' . $_SESSION['organizer']['full-name'] . '<br>' . 
					 'Online Registration #:' . $_SESSION['postid'] . '<br>' .
					 'Paypal Transaction ID: ' . $_POST['transaction_id'] . '<br><br>' .
					 $entry_data;
	// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
