<?php session_start(); ?>

<!-- Use result of DoEC to send the buyer to the correct page -->
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <script>
      (function () {
        if (window != top) {
          top.location.replace('<?= $_SESSION['redirect_url'] ?>');
        } else if (top.window.opener) {
          top.window.opener.location = '<?= $_SESSION['redirect_url'] ?>';
        } else {
          window.location = '<?= $_SESSION['redirect_url'] ?>';
        }
        // console.log(top);
      })();
    </script>
  </head>
  <body>
    <!-- Include non-JavaScript content here -->
    If this page does not redirect <a href="<?= $_SESSION['redirect_url'] ?>">Click Here</a>
  </body>
</html>