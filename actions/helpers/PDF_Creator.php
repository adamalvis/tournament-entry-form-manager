<?php 

	require_once dirname(__FILE__) . '/../../vendor/fpdf/fpdf.php';
    require_once dirname(__FILE__) . '/../../vendor/fpdi/fpdi.php';


	class PDF_Creator {

		private $pdf;
		private $data;
		private $output;


		public function __construct ( $data ) {

			$this->pdf = new FPDI();
			$this->pdf->AddPage();
			$this->pdf->SetFont('Arial','B');


			$this->data = $data;

		}

		public function set_source ( $source_file ) {

			$this->pdf->setSourceFile( $source_file );
			$tplIdx = $this->pdf->importPage( 1 );
    		$this->pdf->useTemplate( $tplIdx, 10, 10, 200 );

		}

		public function set_output ( $output ) {
			$this->output = $output;
		}

		public function generate () {

			foreach ( $this->data as $item ) {
				$this->add_to_pdf( $item );
			}

			$this->pdf->Output('F', $this->output, true);

		}

		private function add_to_pdf ( $item ) {

			switch ( $item['type'] ) {
				case 'text':
					$this->add_text_item( $item );
					break;
				case 'textarea':
					$this->add_textarea_item( $item );
					break;
				case 'check':
					$this->add_check_item( $item );
					break;
				case 'dollar':
					$this->add_dollar_amount( $item );
					break;
			}

		}

		private function add_text_item ( $item ) {

			$this->pdf->SetXY($item['x'], $item['y']);
    		$this->pdf->SetFontSize(9);
    		$this->pdf->Cell(40, 10,$item['data'],0,0,'L');

		}

		private function add_textarea_item ( $item ) {

			$this->pdf->SetXY($item['x'], $item['y']);
    		$this->pdf->SetFontSize(7);
    		$this->pdf->MultiCell(0, 4,$item['data'],0,'L',false);

		}

		private function add_check_item ( $item ) {

			$check_mark = __DIR__ . '/../../static/img/checkmark.png';

			if ( $item['data'] !== '' && $item['data'] !== null ) {
				$this->pdf->Image($check_mark,$item['x'],$item['y'],4,4,'PNG');
			}

		}

		private function add_dollar_amount ( $item ) {

			$data = '$' . $item['data'] . '.00';

			$this->pdf->SetXY($item['x'], $item['y']);
    		$this->pdf->SetFontSize(7);
    		$this->pdf->Cell(40, 10,$data,0,0,'L');

		}

	}