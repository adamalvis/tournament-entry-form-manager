<?php 

	class Bowler_Types {
		private $data;
		const NUM_OF_BOWLERS = 6;
		private $TYPES = array(
			'former-champion' => 'Former Champion',
			'state-officers' => 'State Officers',
			'hof' => 'HOF',
			'pp-iba' => 'PP & IBA Life Members',
			'usbc-board' => 'USBC Board',
			'local-assn-managers' => 'Local Assn Managers',
		);

		public function __construct ($data) {
			$this->data = $data;
		}

		public function pba_bowlers () {

			$pba_bowlers = array();

			for ($i=1; $i <= self::NUM_OF_BOWLERS; $i++) { 
				if ($this->data['bowler-' . $i . '-pba'] === 'yes') {
					array_push($pba_bowlers, $this->data['bowler-' . $i . '-name']);
				}
			}

			return $this->format_pba_bowlers( $pba_bowlers );

		}

		public function other_types () {

			$other_bowlers = array();

			for ($i=1; $i <= self::NUM_OF_BOWLERS; $i++) { 
				if ( isset( $this->data['bowler-' . $i . '-other'] ) ) {
					$types = array(
						'name' => $this->data['bowler-' . $i . '-name'],
						'types' => $this->format_other_types( $this->data['bowler-' . $i . '-other'] ),
					);
					array_push($other_bowlers, $types);
				}
			}

			return $this->format_other_bowlers( $other_bowlers );

		}

		private function format_other_types ( $types ) {

			return $types;

		}

		private function format_other_bowlers ( $bowlers ) {

			$formatted_bowlers = '';

			foreach ($bowlers as $bowler) {
				$formatted_bowlers .= $bowler['name'] . ' (' . $this->TYPES[$bowler['types']] . '), ';
			}

			return $formatted_bowlers;

		}

		private function format_pba_bowlers ( $bowlers ) {

			$formatted_bowlers = '';

			foreach ($bowlers as $bowler) {
				$formatted_bowlers .= $bowler . ', ';
			}

			return $formatted_bowlers;

		}

	}