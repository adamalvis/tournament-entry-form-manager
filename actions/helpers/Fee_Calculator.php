<?php 

	class Fee_Calculator {
		private $data;
		const NUM_OF_TEAMS = 3;

		public function __construct ($post_data) {
			$this->data = $post_data;
		}

		public function doubles_optional_handicap_total () {

			$fee = 3;
			$fee_total = 0;

			for ($i=1; $i <= self::NUM_OF_TEAMS; $i++) { 
				$fee_total += isset( $this->data['doubles-' . $i . '-first-all-event-handicap'] ) ? $fee : 0;
				$fee_total += isset( $this->data['doubles-' . $i . '-second-all-event-handicap'] ) ? $fee : 0;
			}

			return $fee_total;

		}

		public function doubles_optional_actual_total () {

			$fee = 5;
			$fee_total = 0;

			for ($i=1; $i <= self::NUM_OF_TEAMS; $i++) { 
				$fee_total += isset( $this->data['doubles-' . $i . '-first-all-event-actual'] ) ? $fee : 0;
				$fee_total += isset( $this->data['doubles-' . $i . '-second-all-event-actual'] ) ? $fee : 0;
			}

			return $fee_total;

		}

		public function minors_optional_actual () {
			
			$fee = 20;
			$fee_total = 0;

			for ($i=1; $i <= self::NUM_OF_TEAMS; $i++) { 
				$fee_total += isset( $this->data['doubles-' . $i . '-minors-optional-actual'] ) ? $fee : 0;
			}

			return $fee_total;

		}

		public function minors_handicap () {

			$fee = 112;
			$fee_total = 0;

			for ($i=1; $i <= self::NUM_OF_TEAMS; $i++) { 
				if ( isset( $this->data['doubles-' . $i . '-first'] ) && $this->data['doubles-' . $i . '-first'] !== '') {
					$fee_total += $fee;
				}
			}

			return $fee_total;

		}

		public function doubles_team_handicap ($team_num) {

			if ( isset( $this->data['doubles-' . $team_num . '-first'] ) && $this->data['doubles-' . $team_num . '-first'] !== '' ) {
				return true;
			}

			return null;

		}

		public function team_fee () {

			$fee = 140;

			if ( isset( $this->data['team-name'] ) && $this->data['team-name'] !== '' ) {
				return $fee;
			}

			return 0;

		}

		public function team_optional_actual () {

			$fee = 25;

			if ( isset( $this->data['team-optional-actual'] ) && $this->data['team-optional-actual'] ) {
				return $fee;
			}

			return 0;

		}

		public function late_fee () {
			return isset( $this->data['ec-lf'] ) ? 25 : 0;
		}

		public function total_fees () {
			return $this->doubles_optional_handicap_total() +
				   $this->doubles_optional_actual_total() +
				   $this->minors_optional_actual() +
				   $this->minors_handicap() +
				   $this->team_fee() +
				   $this->team_optional_actual() +
				   $this->late_fee();
		}

	}