<?php
    
    require_once dirname(__FILE__) . '/helpers/PDF_Creator.php';
    require_once dirname(__FILE__) . '/data/pdf-data.php';

    // CREATE OUTPUT & SOURCE FILE DATA
    $source_file = __DIR__  . '/../pdfs/source/blank.pdf';
    $output_dir = __DIR__ . '/../pdfs/';
    $file_name = str_replace(' ', '-', $_POST['full-name']);
    $file_name = preg_replace('/[^A-Za-z0-9]/', '', $file_name);
    $file_name .= uniqid();
    $file_name .= '.pdf';
    

    // CREATE PDF

    $pdf_creator = new PDF_Creator($pdf_data);
    $pdf_creator->set_source( $source_file );
    $pdf_creator->set_output( $output_dir . $file_name );
    $pdf_creator->generate();

    $pdf = $file_name;