<?php
	
	// session_start();

	// includes wordpress functions
	require_once __DIR__ . '/../../../../wp-load.php';

	require_once dirname(__FILE__) . '/../pages/index.php';

	// Create Entry Post

	$post_title = $_POST['full-name'] . ' (' . date('m/d/Y') . ')';

	$new_entry = wp_insert_post(array(
		'post_title' => $post_title,
		'post_status' => 'publish',
		'post_type' => 'usbc_entry',
	), false);

	$_POST['postid'] = $new_entry;
	$_SESSION['postid'] = $new_entry;

	// save pdf
	require_once 'save-pdf.php';

	// echo '<pre>'; var_dump($_POST); echo '</pre>';

	$transactions = isset($_SESSION['transactions']) ? $_SESSION['transactions'] : array();

	$organizer = array(
		'full-name' => $_POST['full-name'],
		'reservation-number' => $_POST['reservation-number'],
		'address' => $_POST['address'],
		'city' => $_POST['city'],
		'state' => $_POST['state'],
		'zip-code' => $_POST['zip-code'],
		'evening-phone' => $_POST['evening-phone'],
		'daytime-phone' => $_POST['daytime-phone'],
		'email' => $_POST['email'],
		'local-association' => $_POST['local-association'],
	);

	$late_fee = false;

	if ( isset( $_POST['ec-lf'] ) ) {
		$late_fee = true;
	}

	$_SESSION['organizer'] = $organizer;
	$_SESSION['pdf'] = $pdf;

	array_push( $transactions, array( 
		'id' => $new_entry, 
		'total' => $_POST['ec-value'], 
		'pdf' => $pdf,
		'late_fee' => $late_fee,
	));


	// store new array in transactions
	$_SESSION['transactions'] = $transactions;

	// set status to unpaid
	add_post_meta($new_entry, 'payment-status', 'unpaid');
	add_post_meta($new_entry, 'paypal-transaction-id', '');
	add_post_meta($new_entry, 'amount-paid', '');

	// loop through POST fields and save as post meta to new entry
	foreach ($_POST as $key => $value) {
		add_post_meta($new_entry, $key, $value, true);
	}

	// add post meta for pdf
	add_post_meta($new_entry, 'pdf', $pdf, true);

	// echo '<pre>'; var_dump($_POST); echo '</pre>';

	if ($_POST['submit-action'] === 'checkout') {
		$checkout_url = site_url() . '/' . USBCPages::$PAGES['review-order']['post_name'];
		header('Location: ' . $checkout_url);
	} else {
		header('Location: ' . $_POST['form-url']);
	}