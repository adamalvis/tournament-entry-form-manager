<?php

// includes wordpress functions
require_once __DIR__ . '/../../../../wp-load.php';

header('Content-Type: application/json');

$transactions = isset( $_SESSION['transactions'] ) ? $_SESSION['transactions'] : false;

require_once dirname(__FILE__) . '/mail.php';

foreach ($transactions as $transaction) {
    update_post_meta($transaction['id'], 'payment-status', 'paid');
    update_post_meta($transaction['id'], 'paypal-transaction-id', $_POST['transaction_id']);
    update_post_meta($transaction['id'], 'amount-paid', $_POST['payment_amount']);
}

// email pdf
if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}

// echo '<pre>'; var_dump($_SESSION); echo '</pre>';

// clear session
$_SESSION['transactions'] = array();

echo json_encode(array(
    'total' => $_POST['payment_amount'],
    'transaction_id' => $_POST['transaction_id'],
    'success' => true,
));