<?php

	/**
	 * Plugin Name: OC Tournament Entry Manager
	 * Description: Creates custom tournament registration forms, accepts payments and stores entrant data.
	 * Author: Adam Alvis
	 * Version: 1.0
	 * Author URI: https://adamalvis.com
	 * Text Domain: tournament-registrar
	 * Domain Path: /languages
	 */

	session_start();

	// Exit if accessed directly
	if ( ! defined( 'ABSPATH' ) )
		exit();

	// global plugin path
	define('PLUGIN_DIR', plugin_dir_url(__FILE__));


	require_once 'custom_post_types/create.php';
	require_once 'shortcodes/create.php';
	require_once 'admin/init.php';
	require_once 'pages/index.php';
	// require_once 'models/init.php';

	if ( ! class_exists( 'TournamentEntryManager' ) ) {

		class TournamentEntryManager {

			public function __construct () {
				create_usbc_custom_post_types();
				create_usbc_shortcodes();
				usbc_init_admins();
			}

		}

	}

	/* initiate master class */
	function tournament_entry_manager () {
		return new TournamentEntryManager();
	}

	/* start plugin */
	tournament_entry_manager();

