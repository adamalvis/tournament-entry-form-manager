(function ($) {

	var TestData = {
		
		'full-name': 'Adam Alvis',
		'reservation-number': '1234567',
		'address': '10024 Palermo Circle',
		'city': 'Tampa',
		'state': 'FL',
		'zip-code': '33619',
		'evening-phone': '8124538240',
		'daytime-phone': '8124538240',
		'email': 'adam.alvis@gmail.com',
		'local-association': 'Adams Country USBC (86431)',
		'signup-type': 'both',

		'bowler-1-national-id': 'bowler1id',
		'bowler-1-name': 'Phillip J Fry',
		'bowler-1-email': 'pfry@futurama.com',
		'bowler-1-address': '123 Fake St',
		'bowler-1-city': 'New New York',
		'bowler-1-state': 'NY',
		'bowler-1-zip': '12345',
		'bowler-1-average': '300',
		'bowler-1-pba': 'yes',
		'bowler-1-other': 'former-champion',

		'bowler-2-national-id': 'bowler1id',
		'bowler-2-name': 'Phillip J Fry',
		'bowler-2-email': 'pfry@futurama.com',
		'bowler-2-address': '123 Fake St',
		'bowler-2-city': 'New New York',
		'bowler-2-state': 'NY',
		'bowler-2-zip': '12345',
		'bowler-2-average': '300',
		'bowler-2-pba': 'yes',
		'bowler-2-other': 'former-champion',

		'bowler-3-national-id': 'bowler1id',
		'bowler-3-name': 'Phillip J Fry',
		'bowler-3-email': 'pfry@futurama.com',
		'bowler-3-address': '123 Fake St',
		'bowler-3-city': 'New New York',
		'bowler-3-state': 'NY',
		'bowler-3-zip': '12345',
		'bowler-3-average': '300',
		'bowler-3-pba': 'yes',
		'bowler-3-other': 'former-champion',

		'bowler-4-national-id': 'bowler1id',
		'bowler-4-name': 'Phillip J Fry',
		'bowler-4-email': 'pfry@futurama.com',
		'bowler-4-address': '123 Fake St',
		'bowler-4-city': 'New New York',
		'bowler-4-state': 'NY',
		'bowler-4-zip': '12345',
		'bowler-4-average': '300',
		'bowler-4-pba': 'yes',
		'bowler-4-other': 'former-champion',

		'bowler-5-national-id': 'bowler1id',
		'bowler-5-name': 'Phillip J Fry',
		'bowler-5-email': 'pfry@futurama.com',
		'bowler-5-address': '123 Fake St',
		'bowler-5-city': 'New New York',
		'bowler-5-state': 'NY',
		'bowler-5-zip': '12345',
		'bowler-5-average': '300',
		'bowler-5-pba': 'yes',
		'bowler-5-other': 'former-champion',

		'bowler-6-national-id': 'bowler1id',
		'bowler-6-name': 'Phillip J Fry',
		'bowler-6-email': 'pfry@futurama.com',
		'bowler-6-address': '123 Fake St',
		'bowler-6-city': 'New New York',
		'bowler-6-state': 'NY',
		'bowler-6-zip': '12345',
		'bowler-6-average': '300',
		'bowler-6-pba': 'yes',
		'bowler-6-other': 'former-champion',
		'bowler-6-team': 'BBR',

		'squad-option-one': 'Oct 29, 2016 7:00 PM',
		'squad-option-two': 'Oct 29, 2016 8:00 PM',
		'team-name': 'The Teamers',
		'team-captain': 'Adam Alvis',
		'team-optional-actual': 'checked',

		'squad-option-one-doubles': 'Oct 29, 2016 7:00 PM',
		'squad-option-two-doubles': 'Oct 29, 2016 8:00 PM',

		'doubles-1-first': 'Phillip J Fry',
		'doubles-1-second': 'Phillip J Fry',
		'doubles-1-first-all-event-handicap': 'checked',
		'doubles-1-first-all-event-actual': 'checked',
		'doubles-1-second-all-event-handicap': 'checked',
		'doubles-1-second-all-event-actual': 'checked',
		'doubles-1-minors-optional-actual': 'checked',

		'doubles-2-first': 'Phillip J Fry',
		'doubles-2-second': 'Phillip J Fry',
		'doubles-2-first-all-event-handicap': 'checked',
		'doubles-2-first-all-event-actual': 'checked',
		'doubles-2-second-all-event-handicap': 'checked',
		'doubles-2-second-all-event-actual': 'checked',
		'doubles-2-minors-optional-actual': 'checked',

		'doubles-3-first': 'Phillip J Fry',
		'doubles-3-second': 'Phillip J Fry',
		'doubles-3-first-all-event-handicap': 'checked',
		'doubles-3-first-all-event-actual': 'checked',
		'doubles-3-second-all-event-handicap': 'checked',
		'doubles-3-second-all-event-actual': 'checked',
		'doubles-3-minors-optional-actual': 'checked',

	};

	var FormTester = function () {

		var form = $('#oc-tournament-form'),
			testDataBtn;

		function init () {
			addTestButton();
			registerEvents();
		}

		function registerEvents () {
			testDataBtn.click(function () {
				addTestData();
			});
		}

		function addTestButton () {

			btn = document.createElement('a');

			btn.className = 'test-add-bowler btn';
			btn.innerHTML = 'Add Test Bowlers';

			testDataBtn = $(btn);

			testDataBtn.prependTo(form);

		}

		function addTestData () {
			$.each(TestData, function (key,val) {

				var elem = $('#' + key);

				if (val === 'checked') {
					elem.prop('checked', true);
				} else {
					elem.val(val);
				}

				if (key === 'bowler-6-team') {
					updateDoublesOptions();
				}
			});
		}

		function updateDoublesOptions () {

			var bowlerNames = form.find('.bowler-name'),
				doublesOptions = form.find('.doubles-options'),
				bowlerOptions = '<option></option>';

			bowlerNames.each(function () {
				var name = $(this).val();
				if (name) {
					bowlerOptions += '<option value="' + name + '">' + name + '</option>';
				}
			});

			doublesOptions.each(function () {
				$(this).html(bowlerOptions);
			});

		}

		$(document).ready(function () {
			init();
		});

	}();

})(jQuery);