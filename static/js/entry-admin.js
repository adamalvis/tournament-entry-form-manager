(function ($) {

    $(document).ready(function () {
        
        var postSearchBox = $('#post-search-input');

        // adds descriptive placeholder to post search on entry list
        postSearchBox.attr('placeholder', 'Enter transaction ID');

    });

})(jQuery);