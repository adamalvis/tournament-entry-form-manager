(function ($) {

	var USBC = USBC || {};

	var DEBUG = function () {

		var url = window.location.href;

		if (url.indexOf('usbcba.dev') !== -1) {
			return true;
		}

		return false;

	}();

	USBC.Printer = function (content) {

		var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Printer</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(content);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;

	};

	USBC.ErrorHandler = function (section) {

		var messageContainer = section.find('.form-errors'),

			// error messages
			MESSAGES = {
				'required': '<field> is required.',
				'check': 'Please confirm that you have read the rules before proceeding.',
			};

		function clear (fields) {
			fields.removeClass('error');
			messageContainer.html('');
		}

		function trigger (field, errorType) {

			var message = get_error_message(field, errorType);

			// display error message
			messageContainer.append(message)

			// trigger error on field
			field.addClass('error');

		}

		function get_error_message (field, errorType) {
			var message = MESSAGES[errorType],
				fieldName = get_formatted_field_name(field);
			return message.replace('<field>', fieldName).replace(/\-/g, ' ') + '<br>';
		}

		function get_formatted_field_name (field) {
			var name = field.attr('name');
			return name.charAt(0).toUpperCase() + name.slice(1);

		}

		return {
			clear: clear,
			trigger: trigger,
		};

	};

	USBC.BowlerValidator = function () {

		var bowlers = $('.bowler'),
			signupType = $('#signup-type').val(),
			reqBowlers = {
				0: isRequired(1),
				1: isRequired(2),
				2: isRequired(3),
				3: isRequired(4),
				4: isRequired(5),
				5: isRequired(6),
			};

		function setRequired () {

			var count = 0;

			bowlers.each(function () {

				if (reqBowlers[count]) {
					requireAllFields($(this));
				}

				count++;

			});

		}

		function requireAllFields (bowler) {

			bowler.find('input').addClass('required');

		}

		function isRequired (id) {

			var bowlerName;

			// returns true for first 5 bowlers if signing up a team
			if ( (signupType === 'team' || signupType === 'both') && id < 6 ) {
				return true;
			}

			bowlerName = $('#bowler-' + id + '-name').val();

			if (bowlerName !== null && bowlerName !== '') {
				return true;
			}

			return false;

		}

		return {
			setRequired: setRequired,
		};

	};

	USBC.FormValidator = function () {

		var errorType = 'required';

		function validate (section) {

			var valid = true,
				requiredFields,
				errorHandler = USBC.ErrorHandler(section);

			// do not validate in debug mode
			if (DEBUG || section.hasClass('hide')) {
				return true;
			}

			setRequiredFieldsById(section.attr('id'));

			requiredFields = section.find('.required');

			// clear existing error messages
			errorHandler.clear(requiredFields)

			requiredFields.each(function () {

				var field = $(this);

				if (field.val() === null || field.val() === '' && !field.hasClass('not-required')) {
					valid = false;
					errorHandler.trigger(field, errorType);
				} else if (field.is(':checkbox') && !this.checked) {
					valid = false;
					errorHandler.trigger(field, 'check');
				}

			});

			return valid;

		}

		function setRequiredFieldsById (id) {
			switch (id) {
				case 'bowlers':
					USBC.BowlerValidator().setRequired();
			}
		}

		return {
			validate: validate,
		};

	};

	USBC.FeeCalculator = function () {

		var teamActualBox = $('#team-actual-fees'),
			teamHandicapBox = $('#team-handicap-fees'),
			TeamFees = {
				base: 140,
				optionalActual: 25,
			},
			LATE_FEE = 25.00,
			processingFeeBox = $('#processing-fee'),
			grandTotalBox = $('#grand-total'),
			ecValueBox = $('#ec-value'),
			ecProcessingBox = $('#ec-processing'),
			subTotalBox = $('#sub-total'),
			sdBoxes = {
				sdHandicap: $('#singles-doubles-handicap-fees'),
				sdActual: $('#singles-doubles-actual-fees'),
				allEventHandicap: $('#total-all-event-handicap-fees'),
				allEventActual: $('#total-all-event-actual-fees'),
			},
			PROCESSING_FEE = 0.025,
			totalFees = 0;

		function calculateFees () {
			calculateTeamFees()
			calculateDoublesSinglesFees();
			calculateProcessingFee();
			calculateTotal();
		}

		function calculateTeamFees () {

			var handicap = 0,
				actual = 0,
				optionalActual = document.getElementById('team-optional-actual'),
				teamName = $('#team-name');

			if (teamName.val() !== null && teamName.val() !== '') {
				handicap = TeamFees.base;
			}

			if (optionalActual.checked) {
				actual = TeamFees.optionalActual;
			}

			teamHandicapBox.html('$' + handicap.toFixed(2))
			teamActualBox.html('$' + actual.toFixed(2));
		}

		function calculateDoublesSinglesFees () {

			var teams = $('.doubles-team'),
				total = 0,
				TEAM_FEE = 112,
				sdHandicapFees = 0,
				sdActualFees = 0,
				aeHandicapFees = 0,
				aeActualFees = 0;

			teams.each(function () {

				var partners = $(this).find('.doubles-options');

				if (doublesTeamExists(partners)) {

					sdHandicapFees += TEAM_FEE;

					// add all fee fields
					$(this).find('.fee').each(function () {

						var fee = parseInt(this.value);

						if (this.checked) {

							// split fee types
							switch ($(this).data('fee')) {
								case 'all-event-actual':
									aeActualFees += fee;
									break;
								case 'all-event-handicap':
									aeHandicapFees += fee;
									break;
								case 'optional-actual':
									sdActualFees += fee;
									break;
							}

						}

					});

				}

			});

			sdBoxes.allEventActual.html('$' + aeActualFees.toFixed(2));
			sdBoxes.allEventHandicap.html('$' + aeHandicapFees.toFixed(2));
			sdBoxes.sdHandicap.html('$' + sdHandicapFees.toFixed(2));
			sdBoxes.sdActual.html('$' + sdActualFees.toFixed(2));

		}

		function calculateProcessingFee () {

			var doublesFee = totalDoublesFees(),
				teamFee = totalTeamFees(),
				fee = (doublesFee + teamFee) * PROCESSING_FEE;

			if (isLate()) {
				fee += LATE_FEE * PROCESSING_FEE;
			}

			processingFeeBox.html('$' + fee.toFixed(2));

		}

		function calculateTotal () {

			var doublesFee = totalDoublesFees(),
				teamFee = totalTeamFees(),
				processingFee = parseFloat(processingFeeBox.html().replace('$', '')),
				fee = doublesFee + teamFee;

			ecValueBox.val(fee);
			ecProcessingBox.val(PROCESSING_FEE);

			subTotalBox.html('$' + fee.toFixed(2));

			if (isLate()) {
				fee += LATE_FEE;
			}


			fee += processingFee;

			grandTotalBox.html('$' + fee.toFixed(2));

		}

		function totalDoublesFees () {

			var fees = getBoxValue(sdBoxes.sdHandicap) +
				   getBoxValue(sdBoxes.sdActual) +
				   getBoxValue(sdBoxes.allEventHandicap) +
				   getBoxValue(sdBoxes.allEventActual);
		
			return fees;

		}

		function totalTeamFees () {
			var fees = getBoxValue(teamActualBox) + getBoxValue(teamHandicapBox);
			return fees;
		}

		function getBoxValue (box) {
			return parseFloat(box.html().replace('$', ''));
		}

		function isLate () {
			return $('#ec-lf').length > 0;
		}

		function doublesTeamExists (partners) {

			var exists = true;

			partners.each(function () {
				if ($(this).val() === null || $(this).val() === '') {
					exists = false;
				}
			});

			return exists;

		}


		// run calculations
		calculateFees();

	};

	USBC.PriorYearDates = function() {

		var _priorYearDates,
			_dates;

		function init() {
			_priorYearDates = $('.prior-year');
			_getDates();
			_setDates();
		}

		function _getDates() {
			var date = new Date(),
				year = date.getMonth() > 7 ? date.getFullYear() : date.getFullYear() -1; // at least september
			_dates = (year - 1) + '-' + year;
		}

		function _setDates() {
			_priorYearDates.html(_dates);
		}

		return {
			init: init,
		};

	}();

	USBC.FormController = function () {

		var container,
			form, 
			addBowlersBtn,
			checkoutBtn, 
			errorHandler, 
			bowlerNames, 
			doublesOptions, 
			nextButton, 
			prevButton, 
			currentScreen, 
			formValidator, 
			checkout, 
			signupType,
			printRulesBtn,
			tournamentRules,
			otherSelects,
			finalPage,
			deadline;

		function init () {

			container = $('div.tournament-form');
			form = $('form#oc-tournament-form');
			addBowlersBtn = $('#additional-bowlers-btn');
			checkoutBtn = $('#checkout-btn');
			bowlerNames = container.find('.bowler-name');
			doublesOptions = container.find('.doubles-options');
			nextButton = container.find('a.next');
			prevButton = container.find('a.prev');
			currentScreen = container.find('section.first');
			formValidator = USBC.FormValidator();
			signupType = $('#signup-type');
			doublesScreen = $('#doubles');
			teamScreen = $('#team-info');
			printRulesBtn = $('a.print-rules');
			tournamentRules = $('.tournament-rules');
			otherSelects = $('select[multiple]');
			finalPage = $('#breakdown'),
			deadline = $('#deadline');

			USBC.PriorYearDates.init();

			addLateFee();

			registerEvents();

		}

		function registerEvents () {

			nextButton.click(function (e) {

				e.preventDefault();
				goToNextScreen();

				if ($(this).hasClass('final')) {
					USBC.FeeCalculator();
				}
				
			});

			prevButton.click(function (e) {
				e.preventDefault();
				goToPrevScreen();
			});

			signupType.change(function () {
				hideExtraScreens($(this).val());
			});	

			printRulesBtn.click(function () {
				USBC.Printer(tournamentRules.html());
			});

			otherSelects.change(function () {
				var options = $(this).find('option:selected'),
					section = $(this).closest('.bowler');
				toggleExtraFields(options, section);
			});

			checkoutBtn.click(function (e) {
				e.preventDefault();
				form.submit();
			});

			addBowlersBtn.click(function (e) {
				e.preventDefault();
				$('#submit-action').val('add');
				form.submit();
			});

		}

		function updateDoublesOptions () {

			var bowlerOptions = '<option></option>';

			bowlerNames.each(function () {
				var name = $(this).val();
				if (name) {
					bowlerOptions += '<option value="' + name + '">' + name + '</option>';
				}
			});

			doublesOptions.each(function () {
				$(this).html(bowlerOptions);
			});

		}

		function goToNextScreen () {

			if (formValidator.validate(currentScreen) ) {

				currentScreen.hide();

				if (currentScreen.attr('id') === 'bowlers' && !DEBUG) {
					updateDoublesOptions();
				}

				currentScreen = currentScreen.next();

				if (currentScreen.hasClass('hide')) {
					goToNextScreen();
					return false;
				}

				currentScreen.show();
			}
		}

		function goToPrevScreen () {
			currentScreen.hide();

			currentScreen = currentScreen.prev();

			if (currentScreen.hasClass('hide')) {
				goToPrevScreen();
				return false;
			}

			console.log(currentScreen);

			currentScreen.show();
			
		}

		function hideExtraScreens (type) {

			doublesScreen.removeClass('hide');
			teamScreen.removeClass('hide');
			$('#extra-bowler').show();

			if (type === 'team') {
				doublesScreen.addClass('hide');
				// hide extra bowler
				$('#extra-bowler').hide();
				toggleFinalScreen(type);
			}
			if (type === 'doubles') {
				teamScreen.addClass('hide');
				toggleFinalScreen(type);
			}

		}

		function toggleFinalScreen(type) {

			$('a.next.final').removeClass('final');

			if (type === 'team') {
				$('a.next.team').addClass('final');
			} else {
				$('a.next.doubles-teams').addClass('final');
			}

		}

		function toggleExtraFields (options, section) {

			section.find('.extra-field').hide();

			for (var i = 0; i < options.length; i++) {
				var extraField = $(options[i]).data('extra');
				section.find('.' + extraField).show();
			}

		}

		function addLateFee () {

			var formDeadline = new Date(deadline.val()*1000),
				now = Date.now();

			if  (now < formDeadline.getTime()) {
				console.log('not late!');
			} else {
				console.log('late!');
			}

		}

		$(document).ready(function () {
			init();
		});

	}();

})(jQuery);

